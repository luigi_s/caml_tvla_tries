
type u = U of string | Null

let gI (i:u) = match i with
	| U x -> x
	| Null -> "Null"

let isSameOf (ls1:u list) (ls2:u list) = 
	let isEqF b i1 i2 = b = ((gI i1) = (gI i2)) in
	(List.fold_left2 isEqF true ls1 ls2)
	
type v = V

type p = Un of string | Bn of string | Nil

let ( |=| ) p1 p2 = 
	let getStr (p:p) = match p with
	| Un x -> x
	| Bn x -> x
	| Nil -> "Nil"
	in let compare _p1 _p2 = (getStr _p1) = (getStr _p2)
	in compare p1 p2

type s = 
	| S0 		of (p list) * (u list) 
	| NEW_U 	of s * u
	| NEW_P	 	of s * p  
	| UPD_UNV 	of s * (u -> (u list))
	| UPD_PRE 	of s * (p -> (p list))
	| ITP_FUN	of s * (p -> (u list) -> (p * u list * int) )
	| End



let getStruct (s:s) = match s with
	| S0 (_,_) as root -> root 
	| NEW_U (x,_) -> x
	| NEW_P	(x,_) -> x
	| UPD_UNV (x,_) -> x
	| UPD_PRE (x,_) -> x
	| ITP_FUN (x,_) -> x
	| End -> End

let rec getUniverse (s:s) = 
	match s with
	| S0 (_,y) -> y
	| End -> []
	| _ -> getUniverse (getStruct s)

let rec getPvar (s:s) = match s with
	| S0 (x,_) -> x
	| End -> []
	| _ -> getPvar (getStruct s)

let rec getU_new (s:s) = match s with
	| NEW_U (_,y) -> y
	| End -> Null
	| _ -> getU_new (getStruct s)

let rec getP_new (s:s) = match s with
	| NEW_P (_,y) -> y
	| End -> Nil
	| _ -> getP_new (getStruct s)

let rec getUpd_unv_fun (s:s) = match s with
	| UPD_UNV (_,f) -> f
	| End -> (fun u -> [])
	| _ -> getUpd_unv_fun (getStruct s)

let rec getUpd_pre_fun (s:s) = match s with
	| UPD_PRE (_,f) -> f
	| End -> (fun p -> [])
	| _ -> getUpd_pre_fun (getStruct s)

let rec getItp_fun (s:s) = match s with
	| ITP_FUN (_,f) -> f
	| End -> (fun p ls -> (p, ls, -1))
	| _ -> getItp_fun (getStruct s)

(* Malloc_statemnt *)

(* getS0 *)
let getS0 (pVar : p list) (universe : u list) = S0 (pVar, universe)

(* get S1 *)
let getNEW_U (s:s) = 
	let n_new = U "u_new" in
	NEW_U (s, n_new)

(* get S2 *)
let getNEW_P (s:s) = 
	let isNew = Un "isNew" in
	NEW_P (s, isNew)

(* get S3 *)
let getUPD_UNV (s:s) =
	let universe (u : u) = ([u]@(getUniverse s)) in
	UPD_UNV (s, universe)

(* get S4 *)
let getUPD_PRE (s:s) = 
	let pVar (p:p) = ([p]@getPvar s) in
	UPD_PRE (s, pVar)

(* get S5 *)
let getITP_FUN (s:s) = 
	let i (p:p) (ls : u list) = 
	if ( (p |=| (getP_new s)) && ( isSameOf ls [(getU_new s)])) then (p, ls, 1) else (p, ls, 0) in
	ITP_FUN (s, i)

(* let updateUniverse (universe : u list) = 
	let new_u = (U "u_new") in
	match universe with
	| []  							-> [new_u]
	| [h] when [h] = [new_u] 		-> [U "u1"]
	| [h] 							-> [new_u] @ universe 
	| h::tl when [h] = [new_u] 		-> 
		[(U ("u" ^ (string_of_int ((List.length universe)) )))] @ tl
	| ls  							-> [new_u] @ universe *)

let mkDuo x y = [x;y]
let mkUno x = [x]

let mkMatrix ls = 
let rec mtx ls1 ls2 = 
	match ls1 with 
	| [] -> []
	| [h] -> (List.map (mkDuo h) ls2)
	| h::tl -> (List.map (mkDuo h) ls2) @ (mtx tl ls2)
	in mtx ls ls

let mkVector ls = List.map mkUno ls

let evaluate (s:s) = 
	let updated_Universe = (getUpd_unv_fun s) (getU_new s) in
	let updated_PVar = (getUpd_pre_fun s) (getP_new s) in 
	let matrix = mkMatrix updated_Universe in 
	let vector = mkVector updated_Universe in
	let switch (pred:p) (un_arg : u list list ) (bn_arg : u list list ) = 
		match pred with
		| (Un _) as un -> (List.map ((getItp_fun s) un) un_arg)
		| (Bn _) as bn -> (List.map ((getItp_fun s) bn) bn_arg)
		| _ -> []
		in
		let interpretations = 
			let rec pEva (ls_of_predicates : p list) = 
				match ls_of_predicates with
				| [] -> []
				| [h] -> [(switch h vector matrix)]
				| h::tl -> (switch h vector matrix) :: (pEva tl) 
			in pEva updated_PVar
		in interpretations



(* End **************************** Lastest ********************************* *)

let st_malloc (lhs:p) (universe : u list) (pVar : p list) = 
	let updated_Universe = updateUniverse universe in
	let updated_PVar = updatePVar pVar in
	let matrix = mkMatrix updated_Universe in
	let vector = mkVector updated_Universe in
	let isNew = List.hd updated_PVar in
	let u_new = List.hd updated_Universe in
	let i (p1:p) (ls1 : u list) =
		if ( (p1 |?| isNew) && ( ls1 |??| [u_new])) 
		then (p1, ls1, 1) else (p1, ls1, 0) in
	let switch (un_arg : u list list ) (bn_arg : u list list ) (pred:p) = 
		match pred with
		| (Un _) as un -> (List.map (i un) un_arg)
		| (Bn _) as bn -> (List.map (i bn) bn_arg)
		| _ -> [] in	
	let interpretations (ls_of_predicates : p list) = 
		List.map (switch vector matrix) ls_of_predicates in 
	E (updated_Universe, interpretations updated_PVar)

let evaluate (i : itp) (vocabulary : p list) (universe : u list) = 
 	let updated_Universe = updateUniverse universe in
	let updated_PVar = updatePVar vocabulary in
	let matrix = mkMatrix updated_Universe in
	let vector = mkVector updated_Universe in
	let switch (an_i : itp) (un_arg : u list list ) (bn_arg : u list list ) (pred:p) = 
		match pred with
		| (Un _) as un -> (List.map ((apply an_i) un) un_arg)
		| (Bn _) as bn -> (List.map ((apply an_i) bn) bn_arg)
		| _ -> [] 
		in
	let interpretations (ls_of_predicates : p list) = 
		List.map (switch i vector matrix) ls_of_predicates 
	in interpretations updated_PVar


type call = Call of (store -> action -> store) | Void

type stack = Stack of (call list) 

let push (s:stack) (c:call) = match s with
	| Stack x -> Stack ([c]@x)

let pop (s:stack) = match s with
	| Stack x when x <> [] -> (List.hd x)
	| _ -> Void

(* NOTE:
	

*)


let evaluate_malloc (lhs : p) (pVar : p list) = 
	let forEachPred (arity : u list) (p : p) = 
		(applying (i_malloc lhs i_isNew)) p arity in 
 	let p_star = List.map (forEachPred [(U "u1")]) pVar in
 		p_star

let evaluate_copy (lhs : p) ( rhs : p) (pVar : p list) = 
	let forEachPred (arity : u list) ( p : p ) = 
		(applying (i_copy lhs rhs (i_malloc rhs i_isNew))) p arity in 
	let p_star = List.map (forEachPred [(U "u1")]) pVar in
		p_star 

type action =
	| A_new 		of  itp 
	| A_malloc 		of (p -> itp)
	| A_copy 		of (p -> p -> itp -> itp)
	| A_set_null 	of (p -> itp -> itp)
	| A_get_next 	of (p -> p list -> itp -> u list -> itp)
	| A_set_n_null  of (p -> itp -> itp)
	| A_set_next	of (p list -> p -> itp -> itp)

type store = 
	| S_new 		of	itp
	| S_malloc 		of  p
	| S_copy 		of (p * p * itp)
	| S_set_null 	of (p * itp)
	| S_get_next 	of (p * p list * itp * u list)
	| S_set_n_null  of (p * itp)
	| S_set_next	of (p list * p * itp )
	| Start
	| End

type monad = Action of action


(* let pippo (s:store) = 
	match s with
	| S_new   		 i 			->  i
	| S_malloc 		 p			-> 	p
	| S_copy 		(p1,p2,i)	-> 	p1 p2 i	
	| S_set_null 	(p1, i) 	-> 	p1 i	
	| S_get_next 	(p ,pl,i,u)	-> 	p1 pl i u
	| S_set_n_null 	(p ,i) 		-> 	p  i	
	| S_set_next 	(pl,p ,i)	-> 	pl p i	 *)

(* let ( >>= ) ma mySto = 
	match ma with
	| Action x -> 
		(fun a pip sto -> match a with
		| A_new i			-> pip i sto
		| A_malloc f		-> pip f sto
		| A_copy f			-> pip f sto
		| A_set_null f		-> pip f sto
		| A_get_next f		-> pip f sto
		| A_set_n_null f 	-> pip f sto
		| A_set_next f		-> pip f sto) x pippo mySto 
	 *)


(* let s1_on_x_u1_ = ((applying (itpOf s1) ) x) [u1];;
let s1_on_x_u2_ = ((applying (itpOf s1) ) x) [u2];;

let s1_on_y_u1_ = ((applying (itpOf s1) ) y) [u1];;
let s1_on_y_u2_ = ((applying (itpOf s1) ) y) [u2];;

let s1_on_t_u1_ = ((applying (itpOf s1) ) t) [u1];;
let s1_on_t_u2_ = ((applying (itpOf s1) ) t) [u2];;

let s1_on_e_u1_ = ((applying (itpOf s1) ) e) [u1];;
let s1_on_e_u2_ = ((applying (itpOf s1) ) e) [u2];;

let s1_on_n_u1_ = ((applying (itpOf s1) ) n) [u1;u1];;
let s1_on_n_u1_ = ((applying (itpOf s1) ) n) [u1;u2];;
let s1_on_n_u1_ = ((applying (itpOf s1) ) n) [u2;u1];;
let s1_on_n_u1_ = ((applying (itpOf s1) ) n) [u2;u2];; *)


(* let rec store (st : statement) (pVar : p list) ( s : structure ) = 
	match pVar with
	| [] -> []
	| [h] -> if (getLhs st) = [h] then [(h,(mkStructure_2 s st))] else []
	| h::tl -> if (getLhs st) = [h] 
				then (h , (mkStructure_2 s st)) :: (store (St_copy ((List.hd tl),(List.hd tl)) ) tl (mkStructure_2 s st))
				else (store st (tl @ [h]) s) *)

(* let rec store_2 (st : statement) (ps : (p*structure) list) = 
	let newTail s (ps1 : (p*structure) list) = List.map ((fun z x -> match x with | (p,y) -> (p,z)) s) ps1 in 
	match ps with
	| [] -> []
	| [h] -> if (getLhs st) = [(fst h)] then [((fst h),(mkStructure_2 (snd h) st))] else []
	| h::tl -> if (getLhs st) = [(fst h)] 
				then let newStruct = (mkStructure_2 (snd h) st) in
				let nd = (List.hd tl) in
				((fst h) , newStruct) :: (store_2 (St_copy ( (fst nd) , (fst nd) ) ) (newTail newStruct tl) )
				else (store_2 st (tl @ [h])) *)


				(* let rec eval2 (ls : structure list) (pVar : p list) = 
	let unaryEnvs  = mkVector (universeOf (List.hd ls) ) in
	let binaryEnvs = mkMatrix (universeOf (List.hd ls) ) in
	let mPred s p = 
		match p with
		| Un x as a -> (a, (List.map (applying (itpOf s) a) unaryEnvs))
		| Bn x as b -> (b, (List.map (applying (itpOf s) b) binaryEnvs))
		in
	match ls with
	| [] -> []
	| [h] -> [(h, (List.map (mPred h) pVar))]
	| h::tl -> (h, (List.map (mPred h) pVar)) :: (eval tl pVar) *)


(* let mkStructure ( s : structure ) (st : statement) =
	let structure = match st with
		| St_malloc 	 arg 			-> let newUni = updateUniverse (universeOf s) in
											let newAdd = List.hd newUni in
			 							   S (newUni, (i_malloc arg (itpMOf s newAdd) ))
		| St_copy 		(arg1, arg2) 	-> S (( (universeOf s)), (i_copy arg1 arg2 (itpOf s)))
		| St_set_null 	 arg 			-> S (( (universeOf s)), (i_set_null arg (itpOf s)))
		| St_set_next 	(arg1, arg2) 	-> S (( (universeOf s)), (i_set_next arg1 arg2 (itpOf s)))
		| St_get_next 	(arg1, arg2) 	-> S (( (universeOf s)), (i_get_next arg1 arg2 (itpOf s) (universeOf s)))
		| St_set_n_null  arg 		   	-> S (( (universeOf s)), (i_set_n_null arg (itpOf s)))
	in structure

let rec mkStack (s : structure) (stls : statement list) = 
	match stls with
	| [] -> []
	| [h] -> [mkStructure s h]
	| h::ls -> (mkStructure s h) :: (mkStack (mkStructure s h) ls) *)

(* let rec popItp (p : p) (st : statement) = 
	match st with
	| St_malloc 	 arg 			-> 
	| St_copy 		(arg1, arg2) 	-> 
	| St_set_null 	 arg 			-> 
	| St_set_next 	(arg1, arg2) 	-> 
	| St_get_next 	(arg1, arg2) 	-> 
	| St_set_n_null  arg 		   	->  *)

(* let evStructure ( s : structure ) ( pVar : p list ) = 
	let unaryEnvs = mkVector (universeOf s) in
	let binaryEnvs = mkMatrix (universeOf s) in
	let forEachPred (p : p) = 
		match p with
		| Un x -> (List.map ((applying (itpOf s)) p) unaryEnvs, p , unaryEnvs )
		| Bn x -> (List.map ((applying (itpOf s)) p) binaryEnvs, p , binaryEnvs )
	in List.map forEachPred pVar *)




(* (* x = malloc (...) *)
let l1 = St_malloc x
let s1 = mkStructure S0 l1  
let e1 = evStructure s1 pVar ;; 

(* t = malloc (...) *)
let l4 = St_malloc t
let s1 = mkStructure s1 l4 
let e1 = evStructure s1 pVar ;;

(* y = x *)
let l2 = St_copy (t, x)
let s1 = mkStructure s1 l2 ;;
let e1 = evStructure s1 pVar ;; *)

(* (* t = malloc (...) *)
let l6 = St_malloc y
let s1 = mkStructure s1 l6 
let e1 = evStructure s1 pVar ;; *)

(* (* x = null *)
let l3 = St_set_null y
let s1 = mkStructure s1 l3 ;;
let e1 = evStructure s1 pVar ;; *)

(* y->n = t *)
(* let l5 = St_set_next ([x;n], t)
let s1 = mkStructure s1 l5 ;;
let e1 = evStructure s1 pVar ;; *)

(* (* x->n = t *)
let l5 = St_set_next ([x;n], t)
let s1 = mkStructure s1 l5 ;;
let e1 = evStructure s1 pVar ;;  *)

let rec store_3 (st : statement) (ps : (p*structure) list) = 
	match ps with
	| [] 	-> 	[]
	| [h] 	-> 	if (getLhs st) = [(fst h)] 
				then [((fst h),(mkStructure_2 (snd h) st))] 
				else 
					let newStmt = (St_copy ( (fst h) , (fst h))) in
					if  st = (St_malloc (List.hd (getLhs st)) )
					then 
						let newUni = updateUniverse (universeOf (snd h)) in
						let newAdd = List.hd newUni in
				   		let newStruct = S (newUni, itpOf (snd h)) in 
							[((fst h) , (mkStructure_2 newStruct newStmt))] 
					else  
						[((fst h) , (mkStructure_2 (snd h) newStmt))]
	| h::tl when (getLhs st) <> [(fst h)] -> 	
				let newStmt = (St_copy ( (fst h) , (fst h))) in
				if  st = (St_malloc (List.hd (getLhs st)) )
				then 
					let newUni = updateUniverse (universeOf (snd h)) in
					let newAdd = List.hd newUni in
				 	let newStruct = S (newUni, itpOf (snd h)) in 
						((fst h) , (mkStructure_2 newStruct newStmt)) :: (store_3 st tl)
				else  
					((fst h) , (mkStructure_2 (snd h) newStmt)) :: (store_3 st tl)
	| h::tl when (getLhs st) = [(fst h)] -> ((fst h) , (mkStructure_2 (snd h) st )) :: (store_3 st tl)	



let rec mkItp_2 (st : statement) (universe : u list) = 
	match st with
	| St_malloc 	 arg 			-> i_malloc arg 
	| St_copy 		(arg1, arg2) 	-> i_copy arg1 arg2 
	| St_set_null 	 arg 			-> i_set_null arg 
	| St_set_next 	(arg1, arg2) 	-> i_set_next arg1 arg2 
	| St_get_next 	(arg1, arg2) 	-> i_get_next arg1 arg2 universe 
	| St_set_n_null  arg 		   	-> i_set_n_null arg 


let mkStore (st : statement) (h :  (p * structure)) = 
	(* check if st concerns h's structure *)
	if (getLhs st) = [(fst h)]
	(* make a new structure for h's predicate matching the statement *) 
	then ( (fst h) , (mkStructure_2 (snd h) st) )
	(* st doesn't update h's predicate *) 
	else 
	(* the old itp must be copied over for this predicate *) 
		let newStmt = St_copy ((fst h) , (fst h)) in
	(* is it a malloc statement? (does it require to update the universe ?) *) 
			if   st = St_malloc (List.hd (getLhs st))
			then 
	(* update the universe (appending a new address) *)
				let newUni = updateUniverse (universeOf (snd h)) in
	(* extract the i-th new address just added *)
				let newAdd = List.hd newUni in
	(* create a new interpretation function for evaluating the copied value of this predicate *)
	(* being this a malloc statement, this new itp function will return 1 iff isNew predicate with the new address returns 1 *)
				let innerItp = mkItp st (newUni) (itpOf (snd h))  in
	(* create a new struture with the updated universe and the new itp function *)
				let newStruct = S (newUni, innerItp) in
	(* create the updated structure for this predicate that will represent the copy *)
					((fst h) , (mkStructure_2 newStruct newStmt)) 
			else 
	(* st is not a malloc statement *) 
	(* create a new itp function based on st for the predicate *) 
				let innerItp = mkItp st (universeOf (snd h)) (itpOf (snd h))  in 
	(* create a new struture with the new itp function *)
				let newStruct = S ( (universeOf (snd h)) , innerItp) in	
					((fst h) , (mkStructure_2 newStruct newStmt))

(* let evalu1 = eval a;; *)
(*  
let store1 = List.map (mkStore l1) store0;; 
let evalu1 = eval store1;;

let storeTmp = List.map (mkStore l6) storeTmp;; 
let evalu1 = eval storeTmp;;  *)

(* let store2 = List.map (mkStore l3) store1;; 
let evalu1 = eval store2;;

let store3 = List.map (mkStore l2) store2;; 
let evalu1 = eval store3;; *)

(* 
let storeTmp = List.map (mkStore l4) storeTmp;; 
let evalu1 = eval storeTmp;;

let storeTmp = List.map (mkStore l6) storeTmp;; 
let evalu1 = eval storeTmp;; *) 

(* let a = snd (List.nth storeTmp 0) ;;
let a = itpOf a

let b = (applying a) x [(U "u1")]
let b = (applying a) y [(U "u1")]
let b = (applying a) t [(U "u1")]
let b = (applying a) e [(U "u2")] *)


(* let store1 = store_3 l1 store0 ;;
let evalu1 = eval store1;;

let store2 = store_3 l2 store1 ;;
let evalu2 = eval store2;;

let store3 = store_3 l4 store2 ;;
let evalu3 = eval store3;;

let store4 = store_3 l3 store3 ;;
let evalu4 = eval store4;; *)






let mkStore (st : statement) (pVar : p list) (store : store) = 
	let rec sVar a ls = match ls with
	| [] -> []
	| [h] -> [h]
	| h::tl -> if (h = a) then (h::tl) else sVar a tl@[h] 
	in 
		let s = sStruct store in
		let npVar = 
			if (getLhs st) <> [(List.hd pVar)] 
			then (sVar (List.hd (getLhs st)) pVar) 
			else pVar in 
			let i = itpOf s in
			let uni = universeOf s in 
		let rec copyTail ptl newStruct = 
		match ptl with
		| []    -> store
		| [h]   -> Store ( (mkStructure_2 newStruct (St_copy (h,h)) )  , (copyTail [] newStruct) )
		| h::tl -> Store ( (mkStructure_2 newStruct (St_copy (h,h)) )  , (copyTail tl newStruct) ) 
		in 
		let root = (mkStructure_2 s st) in 
		let updateStruct = 
			if (store <> Begin ) 
			then 
				S (universeOf root , itpOf s )
			else root in 
		Store ( root , (copyTail (List.tl npVar) updateStruct ) )




let rec eval (ls : (p*structure) list) = 
	let unaryEnvs  = mkVector (universeOf (snd (List.hd ls)) ) in
	let binaryEnvs = mkMatrix (universeOf (snd (List.hd ls)) ) in
	let mPred s p = 
		match p with
		| Un x as a -> (a, (List.map (applying (itpOf s) a) unaryEnvs))
		| Bn x as b -> (b, (List.map (applying (itpOf s) b) binaryEnvs))
		in
	match ls with
	| [] 	-> []
	| [h] 	-> [(( mPred (snd h) (fst h) ), (snd h))]
	| h::tl ->  (( mPred (snd h) (fst h) ), (snd h)) :: (eval tl )




let rec newStore (stl : statement list) (s:store) = 
	let nwS str old = Store (str, old) in
	match stl with
	| []    -> End s
	| [h]   -> newStore [] (nwS (mkStructure_2 (sStruct s) h) s )
	| h::tl -> let _new = nwS (mkStructure_2 (sStruct s) h) s in
				newStore tl _new



(* 
S (newUni, (i_malloc arg (itpMOf s newAdd) ))
 *)
let rec mkItp (st : statement) (universe : u list) (i : itp) = 
	match st with
	| St_malloc 	 arg 			-> i_malloc arg i
	| St_copy 		(arg1, arg2) 	-> i_copy arg1 arg2 i
	| St_set_null 	 arg 			-> i_set_null arg i
	| St_set_next 	(arg1, arg2) 	-> i_set_next arg1 arg2 i
	| St_get_next 	(arg1, arg2) 	-> i_get_next arg1 arg2 universe i
	| St_set_n_null  arg 		   	-> i_set_n_null arg i



let evalStore (stores : store) (pVar : p list) = 
	let unaryEnvs  = mkVector (universeOf (sStruct stores)) in
	let binaryEnvs = mkMatrix (universeOf (sStruct stores)) in
	let mPred s p = 
		match p with
		| Un x as a -> (a, (List.map ((sItp s) a) unaryEnvs))
		| Bn x as b -> (b, (List.map ((sItp s) b) binaryEnvs))
		in
	let rec forEachStore sto = if (sto <> Begin) then (sto, (List.map (mPred sto) pVar)) else (forEachStore (sStore sto))
in forEachStore stores

let evalStore1 (stores : store) (pVar : p list) = 
	let unaryEnvs ss = mkVector (universeOf (sStruct ss)) in
	let binaryEnvs ss = mkMatrix (universeOf (sStruct ss)) in
	let mPred s p = 
		match p with
		| Un x as a -> (a, (unaryEnvs s) , (List.map ((sItp s) a) (unaryEnvs s)  ))
		| Bn x as b -> (b, (binaryEnvs s), (List.map ((sItp s) b) (binaryEnvs s) ))
		in
	let rec forEachPred sto ls = 
		match ls with
		| [] -> []
		| [h] -> [(mPred sto h)]
		| h::tl -> (mPred sto h) :: (forEachPred (sStore sto) tl) 
	in let rec forEachStore sto1 pVar = 
		if ( sto1 = Begin ) then [] else (forEachPred sto1 pVar) :: (forEachStore (sStore sto1) pVar)
	in forEachStore stores pVar




	(* let rec f1 pl s = 
	match pl with
	| [] 	-> []
	| [h] 	-> [ (h, nsItp s, ( (sItp s) h [(U "u1")] )) ]
	| h::tl ->   (h, nsItp s, ( (sItp s) h [(U "u1")] )) :: ( f1 tl (sStore s) )

let rec f2 pl s = 
	match pl with
	| [] 	-> []
	| [h] 	-> [ (mkStructure_2 (sStruct s) (St_copy (h,h)))   ] 
	| h::tl ->   (mkStructure_2 (sStruct s) (St_copy (h,h))) :: ( f2 tl (sStore s) )

let rec f3 pl sl = 
	match pl with
	| [] 	-> []
	| [h] 	-> [ (mkStructure_2 (List.hd sl) (St_copy (h,h)))   ] 
	| h::tl ->   (mkStructure_2 (List.hd sl) (St_copy (h,h))) :: ( f3 tl (List.tl sl) )

let rec f4 pl s uls = 
	match pl with
	| [] 	-> []
	| [h] 	-> [ ( applying ( itpOf (List.hd s)) h uls ) ] 
	| h::tl ->   ( applying ( itpOf (List.hd s)) h uls ) :: ( f4 tl (List.tl s) uls )

let a1 = f1 pVar a ;;
let a2 = f2 pVar a ;;
let a3 = f3 pVar a2 ;;
let a4 = f4 pVar a3 [(U "u1")] ;;
let a4 = f4 pVar a3 [(U "u2")] ;; *)



(* let c = evalStore1 b pVar;; 
 *)

(* let a = mkStore l1 pVar Begin ;;
let b = evalStore a pVar;; *)

(*let a = mkStore l3 pVar a ;;
let b = evalStore a pVar;;

let a = mkStore l2 pVar a ;;
let b = evalStore a pVar;; *)

(* let a = newStore stList Begin ;; *)
(* let b = evalStore a pVar;; *)


(* let a = i_isNew [u1];;
let z = (applying a) (Un "isNew") [u1] ;;
let z = (applying a) (Un "isNew") [u2] ;;
let z = (applying a) x [u1] ;;

let b = i_copy x (Un "isNew") (i_isNew [u1]) ;;
let z = (applying b) x [u1] ;; 
let z = (applying b) x [u2] ;; 

let c = i_copy x y (i_malloc y (i_isNew [u1])) ;;
let z = (applying c) x [u1] ;;
let z = (applying c) x [u2] ;;

let d = i_malloc x (i_isNew [u1]) ;;
let z = (applying d) x [u1] ;;
let z = (applying d) x [u2] ;;
let z = (applying d) y [u1] ;;
let z = (applying d) y [u2] ;;

let f = i_malloc t (i_isNew [u2]) ;;
let z = (applying f) t [u1] ;;
let z = (applying f) t [u2] ;;

let g = i_set_next [x;n] t f ;;
let z = (applying g) n [u1;u1] ;;
let z = (applying g) n [u1;u2] ;;
let z = (applying g) n [u2;u1] ;;
let z = (applying g) n [u2;u2] ;; *)


(* let i_get_next (lhs : p) (rhs : p list) (universe : u list) (i : itp) = 
	let fsP = List.hd rhs in
	let ndP = List.nth rhs 1 in
	let tmp (p1:p) (ls0 : u list) = 
		let rec inTail (u1:u) (u_ls:u list) = match u_ls with	
		| []    -> 	false
		| [h]   -> 	(((applying i) ndP [u1;h]) = 0) && ([h] = ls0)  
		| h::tl -> 	if (((applying i) ndP [u1;h]) = 0) && ([h] = ls0) 
					then true
					else inTail u1 tl in
		let rec check (ls1 : u list) = match ls1 with
		| []    ->  false
		| [f]   -> 	if ((applying i) fsP [f]) = 0 			then false 
					else if ((applying i) ndP [f;f]) = 0 	then false else true 
		| f::xs -> 	if (((applying i) fsP [f]) = 1) && (((applying i) ndP [f;f]) = 1) 
					then true (* is reflexive *) 
					else if (inTail f xs)				then true else (check xs) 	
		in 	if (p1 <> lhs)
			then (applying i) p1 ls0  
			else if (check universe) then 1 else 0 in 
	Get_next tmp *)






(* let collect oldStorel pVar block = 
	let wh_acc = 0 in
	let if_acc = 0 in
	let rec take oldStorel1 pVar1 block1 stl whacc ifacc = 
	match block with
	| While (s, b, xt)	-> 	if isEnd s && whacc = 0
							then 
								stl
							else 
								if (evalCondition (List.hd oldStorel1) pVar1 s)
								then take oldStorel1 pVar1 b stl (whacc+1) ifacc
								else take oldStorel1 pVar1 b stl (whacc+1) ifacc
	| Body (sl, b)		-> 	let newList = (_BODYstores 	oldStorel pVar block) in
								_WHILEstores newList pVar b block2
	| If (s,tb,fb,tx)	-> 	let newList = (_IFstores 	oldStorel pVar block) in
							let lastStore = List.hd newList in
							if evalCondition lastStore pVar s 
							then _WHILEstores newList pVar tb block2
							else _WHILEstores newList pVar fb block2
							
	| Start (voc , b) 	-> 	_WHILEstores 				oldStorel voc b block2
	| _ -> oldStorel  *)