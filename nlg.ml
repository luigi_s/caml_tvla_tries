
type u = U of string

let gI (i:u) = match i with
	| U x -> x

type p = Un of string | Bn of string | IsNew | Null

let updateUniverse (universe : u list) = [(U ("u" ^ (string_of_int ((List.length universe)+1) )))] @ universe
		
let updateVocabulary (pVar : p list) = 
	let isNew = (Un "isNew") in
	match pVar with
	| [] -> []
	| h::tl when h = isNew -> tl
	| _ -> [isNew] @ pVar

let mkMatrix ls = 
	let mkDuo x y = [x;y] in
	let rec mtx ls1 ls2 = 
		match ls1 with 
		| [] -> []
		| [h] -> (List.map (mkDuo h) ls2)
		| h::tl -> (List.map (mkDuo h) ls2) @ (mtx tl ls2)
	in mtx ls ls

let fvMatrix ls v = 
	let mkDuo x y = 
		match v with
		 	| [] -> []
		 	| [h] -> [h;y] 	
		 	| h::tl -> [h;y]
		in List.map (mkDuo v) ls

let fvMatrixL ls v = 
	let mkDuo x y = 
		match v with
		 	| [] -> []
		 	| [h] -> [y;h] 	
		 	| h::tl -> [y;h]
		in List.map (mkDuo v) ls

let mkVector ls = 
	let mkUno x = [x] in 
	List.map mkUno ls



type statement = 
	| St_malloc 	of p 
	| St_set_null 	of p 
	| St_copy 		of p * p  
	| St_get_next 	of p * p list
	| St_set_next 	of p list * p
	| St_set_n_null of p list
	| St_isEqual 	of p list * p list
	| St_isNotEq	of p list * p list
	| St_end

let isEnd s = match s with
	| St_end -> true	
	| _ -> false

let getLhs st = 
	match st with
	| St_malloc 	 lhs 		-> [lhs]
	| St_set_null 	 lhs 		-> [lhs]
	| St_copy 		(lhs, rhs) 	-> [lhs] 
	| St_get_next 	(lhs, rhs) 	-> [lhs]
	| St_set_next 	(lhs, rhs) 	->  lhs
	| St_set_n_null  lhs 		->  lhs
	| St_isEqual 	(lhs, rhs) 	->  lhs
	| St_isNotEq	(lhs, rhs) 	->  lhs  
	| St_end					-> []

let getRhs st = 
	match st with
	| St_copy 		(lhs, rhs) 	-> [rhs] 
	| St_get_next 	(lhs, rhs) 	->  rhs
	| St_set_next 	(lhs, rhs) 	-> [rhs]
	| St_isEqual 	(lhs, rhs) 	->  rhs 
	| St_isNotEq	(lhs, rhs) 	->  rhs  
	| _ 						-> []

let mkPVar (stl : statement list) = 
	let rec join ls1 =
	match ls1 with
	| [] -> []
	| [h] -> (getLhs h)
	| h::tl -> (getLhs h) @ (join tl)
	in let rec rem_double ls2 = 
	match ls2 with
	| [] -> []
	| [h] -> [h]
	| h::tl -> [h]@(rem_double (List.filter ((<>) h) tl ))
	in rem_double (join stl)

type itp =
	| New 			of (p -> u list -> int)
	| Malloc 		of (p -> u list -> int)
	| Copy 			of (p -> u list -> int)
	| Set_null 		of (p -> u list -> int)
	| Get_next 		of (p -> u list -> int)
	| Set_n_null 	of (p -> u list -> int)
	| Set_next 	 	of (p -> u list -> int)
	| Init 			

let applying (i : itp) = match i with
	| New 			i -> i
	| Malloc 		i -> i
	| Copy 			i -> i
	| Set_null 		i -> i
	| Get_next 		i -> i
	| Set_n_null 	i -> i
	| Set_next 	 	i -> i
	| _ 			  -> (fun p ls -> 0) 

(* NOTE: 
	
	- tmp is the actual interpretaion function that will assign 1 or 0 or 1/2.
	- Which environment will evaluate to 1 or 0 or 1/2 must be stored at calling time
	(i.e. (applying i) ~predicate ~environment -> 0/1/0.5
	 so that when it is called only what actual environment for a predicate is known, 
	 as only after it is known the evaluation value)
	- these functions are designed to be called on any predicate and any environment,
	so that they can be mapped over a set of predicates and a combinatory space of environments
	(given an universe) 

	(i.e.
		
		universe [u1,u2,u3]
		binary predicate n(x1,x2)

		List.map (applying i) n [[u1,u1],[u1,u2],[u1,u3],
								 [u2,u1],[u2,u2],[u2,u3],
								 [u3,u1],[u3,u2],[u3,u3]]

		it will return a list of 9 integers in {1, 0, 0.5} 
		(given an itp function for each store of the program)
	)

	S1 ( [ u1 ] 			,  	i_malloc with (lhs=x) & i_isNew(ls1 = u1) )
								in itpOf S1:
	E 	[ 	x(u1), -----> 		x=lhs & ls1=u1 -> 1
			y(u1), -----> 		y≠lhs -> 0
			t(u1), -----> 		t≠lhs -> 0
			e(u1), ----->		e≠lhs -> 0
			n(u1, u1) -->  		n≠lhs -> 0
		]

	S ( [ u2 , u1]			,  i_malloc with (lhs=y) & i_isNew(ls1 = u2) )
	
	E 	[ 	x(u1), -----> 		x≠lhs -> 0
			y(u1), -----> 		y=lhs & ls1≠u1 -> 0
			t(u1), -----> 		t≠lhs -> 0
			e(u1), ----->		e≠lhs -> 0
			x(u2), -----> 		x≠lhs -> 0
			y(u2), -----> 		y=lhs & ls1=u2 -> 1
			t(u2), -----> 		t≠lhs -> 0
			e(u2), ----->		e≠lhs -> 0
			n(u1, u1) -->  		n≠lhs -> 0
			n(u1, u2) -->  		n≠lhs -> 0
			n(u2, u1) -->  		n≠lhs -> 0
			n(u2, u2) -->  		n≠lhs -> 0
		]
*)


(* HOW TO READ *)

(* copy, on lhs, the evaluation value - integer - that rhs has given an interpretaion function i *)
let i_copy (lhs : p) (rhs : p) (i : itp) = 
	let tmp (p1:p) (ls1 : u list) =
		if (p1 = lhs) 
		then (applying i) rhs ls1  
		else 0  in 
	Copy tmp 

let i_isNew (u : u list) =  
	(* let isNew = (Un "isNew") in *)
	let tmp (p1:p) (ls1 : u list) =
		if ( (p1 = IsNew) && ( ls1 = u)) 
		then 1 else 0 in 
	New tmp 

let i_malloc (lhs : p) (i : itp) = 
	(* let isNew = (Un "isNew") in *)
	let tmp (p1 : p) (ls1 : u list) =
		if (lhs = p1) 
		then let copy = i_copy lhs IsNew i in (applying copy) lhs ls1 
		else 0 in
	Malloc tmp

let i_set_null (lhs : p) = 
	let tmp (p1:p) (ls1 : u list) = 0 
		in
	Set_null tmp 

let i_get_next (lhs : p) (rhs : p list) (universe : u list) (i : itp list) =
	let p_st 	= List.hd 		rhs 		in
	let p_nd 	= List.nth 		rhs 	1 	in 
	let i_nd 	= List.hd 		i 		 	in
	let i_rd 	= List.nth 		i 		1 	in
	let eXi i_1 p_1 env = if ((applying i_1) p_1 env) = 1 then true else false in
		let rec overVector i_2 p_2 vec = match vec with
		| [] -> []
		| [h] -> if (eXi i_2 p_2 h) then h else []
		| h::tl -> if (eXi i_2 p_2 h) then h else overVector i_2 p_2 tl
		in
	let tmp (p1 : p) (ls1 : u list) = 
		if p1 = lhs
		then 	let v1 = ( overVector i_nd p_st (mkVector universe) ) in
				if v1 = []
				then 0 
				else 	if 	(overVector i_rd p_nd (fvMatrix universe v1)) = (v1@ls1)
						then 1
						else 0
		else 0 in
	Get_next tmp

let i_set_n_null (lhs : p list) (i : itp list) = 
	let mPred p ls = match p with
		| Un x 	-> 0
		| Bn x 	-> 
			let fsP = List.hd lhs 		in
			let ndP = List.nth lhs 1 	in
			let i_st = List.hd i 		in
			let i_nd = List.nth i 1 	in
			if (p = ndP) && (((applying i_nd) ndP ls) = 1) && ( ((applying i_st) fsP [(List.hd ls)] ) = 1 )
			then 0
			else 0 
		| _ 	-> 0
		in 
		let tmp (p1:p) (ls1 : u list) = mPred p1 ls1 in
	Set_n_null tmp

let i_set_next (lhs : p list) (rhs : p ) (i : itp list) = 
	let p_lhs_1	= List.hd 		lhs 		in
	let p_lhs_2	= List.nth 		lhs 	1 	in 
	let i_lhs_1	= List.hd 		i 			in 
	let i_lhs_2	= List.nth 		i 		1 	in
	let i_rhs 	= List.nth 		i 		2 	in
	let tmp (p1:p) (ls1 : u list) =
		if ( p1 = p_lhs_2)
		then 	
			let v1 = List.hd ls1 								in
			let v2 = List.nth ls1 1 							in 
			let sel_v1_v2 = ((applying i_lhs_2) p1 ls1) 	= 1 in
			let x_v1 = ((applying i_lhs_1) p_lhs_1 [v1])	= 1 in 
			let t_v2 = ((applying i_rhs) rhs [v2]) 			= 1 in		
			if (sel_v1_v2 || (x_v1 && t_v2)) then 1 else 0
		else 0 in
	Set_next tmp

type structure = S of u list * itp | CS of u list * itp list

let itpOf s = match s with
	| S 	( _ , i ) -> i 
	| CS 	( _ , i ) -> List.hd i 

let itpsOf s = match s with
	| S 	( _ , i ) -> [i] 
	| CS 	( _ , i ) -> i 

let universeOf s = match s with
	| S 	( u , _ ) -> u 
	| CS 	( u , _ ) -> u

let u_new s = match s with
	| S 	( u , i ) -> S  ( (updateUniverse u) , i )
	| CS 	( u , i ) -> CS ( (updateUniverse u) , i )
	
let mkStructure ( s : structure ) ( st : statement ) =
		let structure = match st with
		| St_copy 		(arg1, arg2) 	-> S ( (universeOf s) , ( i_copy 		arg1 	arg2 						( itpOf s)))
		| St_set_null 	 arg 			-> S ( (universeOf s) , ( i_set_null 	arg 								))
		| St_set_next 	(arg1, arg2) 	-> S ( (universeOf s) , ( i_set_next 	arg1 	arg2 						(itpsOf s)))
		| St_get_next 	(arg1, arg2) 	-> S ( (universeOf s) , ( i_get_next 	arg1 	arg2 	(universeOf s) 		(itpsOf s)))
		| St_set_n_null  arg 		   	-> S ( (universeOf s) , ( i_set_n_null 	arg 								(itpsOf s)))
		| St_malloc 	 arg 			-> let newUni = updateUniverse (universeOf s) in
										   let newAdd = List.hd newUni in
			 							   S (  newUni		  , ( i_malloc 		arg 								(i_isNew [newAdd])))
		| _ 							-> S ([], Init)
	in structure

type store = Store of structure * store | End of store | Empty

let rec sStruct s = match s with
	| Store (s , _ ) -> s
	| End x -> sStruct x
	| _ -> S ([], Init)

let rec sStore s = match s with
	| Store (_ , s ) -> s
	| End x -> sStore x
	| _ -> Empty

let rec sItp s = match s with
	| Store (x , _) -> applying (itpOf x)
	| End x -> sItp x
	| _ -> applying Init

let rec nsItp s = match s with
	| Store 	(x , _) 	-> (itpOf x)
	| End 		 x 			-> nsItp x
	| _ 					-> Init

let rec lst ls = match ls with
	| [] -> Empty
	| [h] -> h
	| h::tl -> lst tl

(* Start ************************** Search ******************************** *)

let rec findS pVar p store = match pVar with
	| [] 	-> S ((universeOf (sStruct store)), Init)
	| [h] 	-> if ( p = [h] ) then (sStruct store) else S ((universeOf (sStruct store)), Init)
	| h::tl -> if ( p = [h] ) then (sStruct store) else findS tl p (sStore store)

let rec findStore pVar p store = match pVar with
	| [] 	-> store
	| [h] 	-> if ( p = [h] ) then (store) else (store)
	| h::tl -> if ( p = [h] ) then (store) else findStore tl p (sStore store)

let findSM pVar pl store = 	
	let rec createItps ls = match ls with
	| [] ->[Init]
	| [h] -> [(itpOf (findS pVar [h] store))]
	| h::tl -> (itpOf (findS pVar [h] store)) :: (createItps tl) 
	in CS ( (universeOf (sStruct store) ) , (createItps pl) ) 

(* End **************************** Search ********************************* *)

let mkStore (stmt : statement) (vocabulary : p list) (input : store) = 
	let actionOn stM stO p f ls = match stM with
	| St_malloc x   	-> 	if x = p 
							then Store ( (mkStructure 		(sStruct stO) 					 		stM     ) , f stM ls ( sStore stO ) ) 
							else Store ( (mkStructure (u_new(findS vocabulary [p] input )) 	(St_copy (p,p)) ) , f stM ls ( sStore stO ) )  
	| St_copy (x,y) 	-> 	if x = p 
							then Store ( (mkStructure 		(findS vocabulary [y] input )  	 		stM     ) , f stM ls ( sStore stO ) ) 
							else Store ( (mkStructure 		(findS vocabulary [p] input ) 	(St_copy (p,p)) ) , f stM ls ( sStore stO ) )
	| St_set_null x 	-> 	if x = p
							then Store ( (mkStructure 		(sStruct stO)				  	 		stM     ) , f stM ls ( sStore stO ) )
							else Store ( (mkStructure 		(findS  vocabulary [p] input ) 	(St_copy (p,p)) ) , f stM ls ( sStore stO ) )
	| St_get_next (x,y) ->  if x = p
							then Store ( (mkStructure 		(findSM vocabulary  y input ) 	 		stM     ) , f stM ls ( sStore stO ) )
							else Store ( (mkStructure 		(findS  vocabulary [p] input ) 	(St_copy (p,p)) ) , f stM ls ( sStore stO ) )
	| St_set_next (x,y) ->  if (List.nth x 1) = p
							then Store ( (mkStructure 		(findSM vocabulary (x@[y]) input ) 	 	stM     ) , f stM ls ( sStore stO ) )
							else Store ( (mkStructure 		(findS  vocabulary [p] input ) 	(St_copy (p,p)) ) , f stM ls ( sStore stO ) )
	| St_set_n_null x 	->	if (List.nth x 1) = p 
							then Store ( (mkStructure 		(findSM vocabulary x   input ) 	 		stM     ) , f stM ls ( sStore stO ) ) 
	 						else Store ( (mkStructure 		(findS  vocabulary [p] input ) 	(St_copy (p,p)) ) , f stM ls ( sStore stO ) )
	| _ 				->  Empty
	in
	let rec mkTmp (st : statement) (pVar : p list) (store : store) = 
		match pVar with
		| [] 	-> 	store
		| [h] 	-> 	actionOn st store h mkTmp []
		| h::tl -> 	actionOn st store h mkTmp tl
		in mkTmp stmt vocabulary input

(* Start ************************** Evaluate ******************************** *)

let rec getInd store p pVar = 
	let eXi i_1 p_1 env = if ((applying i_1) p_1 env) = 1 then true else false in
	let rec overVector i_2 p_2 vec = match vec with
	| [] -> []
	| [h] -> if (eXi i_2 p_2 h) then h else []
	| h::tl -> if (eXi i_2 p_2 h) then h else overVector i_2 p_2 tl
	in
	let mP p2 store1 = match p2 with
		| Null -> []
		| Un x -> overVector (itpOf(findS pVar [p2] store1))  p2 (mkVector (universeOf(sStruct store1)))
		| Bn x -> let env = ( overVector (itpOf((findS pVar [p2] store1))) p2 (mkMatrix (universeOf(sStruct store1)) ) ) in
				  if (List.length env) > 1
				  then [(List.nth env 1)] else []
		| _ -> [(List.hd (universeOf(sStruct store1)))]
	in
	match pVar with
	| [] -> []
	| [h] -> if h = p then mP h store else []
	| h::tl -> if h = p then mP h store else (getInd (sStore store) p tl)	

let checkPairs store pl pVar = 
	let eXi i_1 p_1 env = if ((applying i_1) p_1 env) = 1 then true else false in
	let rec overVector i_2 p_2 vec = match vec with
	| [] -> []
	| [h] -> if (eXi i_2 p_2 h) then h else []
	| h::tl -> if (eXi i_2 p_2 h) then h else overVector i_2 p_2 tl
	in
		let p1 = List.hd pl in 
		let p2 = (List.nth pl 1) in 
		let v1 = ( overVector (itpOf((findS pVar [p1] store))) p1 (mkVector (universeOf(sStruct store)) ) ) in
		let v2 = if v1 <> [] && ( overVector (itpOf((findS pVar [p2] store))) p2 (fvMatrix (universeOf(sStruct store)) v1 ) ) <> []
					then true else false
	in v2

let rec eEnvVSpl store pls uls = 
	match pls with
	| [] -> []
	| [h] -> [(sItp store) h uls]
	| h::tl -> (( sItp store) h uls ) :: (eEnvVSpl (sStore store) tl uls)

let evalStore pls store =
	let mP f p ls = match p with
	| Un x 	-> (List.map (f p) (mkVector ls) )
	| Bn x 	-> (List.map (f p) (mkMatrix ls) )
	| _ 	-> (List.map (f p) [])
	in
	let rec f3 store1 pls1 = 
	match pls1 with
	| [] -> []
	| [h] -> [ (mP (sItp store1) h (universeOf (sStruct store1))) ]
	| h::tl -> (mP (sItp store1) h (universeOf (sStruct store1))) :: (f3 (sStore store1) tl)
	in f3 store pls 

let evalPred store pls p =
	let mP f p ls = match p with
	| Un x 	-> (List.map (f p) (mkVector ls) )
	| Bn x 	-> (List.map (f p) (mkMatrix ls) )
	| _ 	-> (List.map (f p) [])
	in
	let rec f3 store1 pls1 = 
	match pls1 with
	| [] -> []
	| [h] -> if (h = p) then (mP (sItp store1) h (universeOf (sStruct store1))) else []
	| h::tl -> if (h = p) then (mP (sItp store1) h (universeOf (sStruct store1))) else (f3 (sStore store1) tl)
	in f3 store pls 								

let evalCondition store pls st = 
	let rec pOf pl = match pl with
	| [] -> Null
	| [h] -> h
	| h::tl -> pOf tl 
	in
	match st with
	| St_isEqual (lhs , rhs) when (pOf rhs) = Null && (List.length lhs > 1)	->  not (checkPairs store lhs pls)
	| St_isEqual (lhs , rhs) when (pOf rhs) = Null 	-> if (getInd store (pOf lhs) pls ) = [] then true else false
	| St_isEqual (lhs , rhs) when List.length rhs > 1 || List.length lhs > 1 -> 
														if (getInd store (pOf rhs) pls) = (getInd store (pOf lhs) pls) then true else false
	| St_isEqual (lhs , rhs) -> if (evalPred store pls (pOf lhs)) = (evalPred store pls (pOf rhs)) then true else false
	
	| St_isNotEq (lhs , rhs) when (pOf rhs) = Null && (List.length lhs > 1)	-> (checkPairs store lhs pls)													
	| St_isNotEq (lhs , rhs) when (pOf rhs) = Null 	-> if (getInd store (pOf lhs) pls ) <> [] then true else false
	| St_isNotEq (lhs , rhs) when List.length rhs > 1 || List.length lhs > 1 -> 
														if (getInd store (pOf rhs) pls) <> (getInd store (pOf lhs) pls) then true else false
	| St_isNotEq (lhs , rhs) -> if (evalPred store pls (pOf lhs)) <> (evalPred store pls (pOf rhs)) then true else false
	| _ -> false

(* End **************************** Evaluate ********************************* *)

(* Start ************************** Flow Control ******************************** *)

type block = 
	| If 	of statement 		* block * block 
	| While of statement 		* block * block
	| Body 	of statement list	* block		
	| EIF  	of block
	| EWH	of block
	| Exit

let gGuard bl = 
	match bl with
	| If (c, tb, fb) -> c
	| While (c, tb, fb) -> c
	| _ -> St_end

let rec runAll stl store pVar = 
	match stl with
	| [] -> []
	| [h] -> [(mkStore h pVar store)]
	| h::tl -> 	let newStore = (mkStore h pVar store) in 
				[newStore]@(runAll tl newStore  pVar)

let reset store = 
	let y = Un "y" in 
	let t = Un "t" in 
	let e = Un "e" in 
	runAll [(St_set_null y);(St_set_null t);(St_set_null e)] store 

let rec call bl _wh _if roots pVar sol = 
	match bl with		  
	| While (c, tb, fb) -> 	if 		(evalCondition (lst sol) pVar c)	 
							then 	(call tb (_wh+1) _if ([bl]@roots) pVar sol )
							else 	(call fb _wh _if roots pVar sol)

	| If 	(c, tb, fb) -> 	if 		(evalCondition (lst sol) pVar c) 
							then 	(call tb _wh (_if+1) roots pVar sol )
							else 	(call fb _wh _if roots pVar sol)		

	| Body 	(x, next) 	-> 	let newStores = sol	@ (runAll x (lst sol) pVar) in
									(call next _wh _if roots pVar newStores)
 	
 	| EIF 	next		-> 			(call next _wh (_if - 1) roots pVar sol)

	| EWH 	next 		-> 	if 		(evalCondition (lst sol) pVar (gGuard (List.hd roots)))
							then 	(call (List.hd roots) _wh _if (List.tl roots) pVar sol)
							else 	(call next (_wh - 1) _if (List.tl roots) pVar sol)
						
	| Exit				-> 	sol @ ((reset (lst sol)) pVar)


(* End **************************** Flow control ********************************* *)


(* Start ************************** Test ******************************** *)

let x = Un "x";;
let y = Un "y";;
let t = Un "t";;
let e = Un "e";;
let n = Bn "n";;

let pVar = [ y; t; e; x; n] ;;

(* let l1 = St_malloc x
let l2 = St_copy (y, x)
let l12 = St_set_n_null ([y;n])

let guard1 = St_isNotEq ([y;n], [Null])

let l11 = St_get_next (y, [y;n])

let guard2 = St_isNotEq ([x], [Null])

let l4 = St_malloc t

let guard3 = St_isEqual ([x], [t])

let l9 = St_get_next (x, [y;n])

let l3 = St_malloc e
let l7 = St_set_null x 
let l6 = St_copy (e, x)
let l8 = St_copy (t, e)
let l5 = St_set_next ([y;n], t)
let l10 = St_set_n_null [y;n]



let g3True		= Body 	( [l4]	 		,  EIF Exit							)
let g1False		= If 	( guard3 		,  g3True 			, EIF Exit		)

let g2True   	= Body 	( [l4]	 		, (EIF (EWH g1False))				)
let if1			= If 	( guard2 		,  g2True 			, g1False		)
let g1True 		= Body 	( [l11]  		,  if1 								)

let w1 			= While ( guard1  		,  g1True 			, g1False		)
let graph  		= Body  ( [l1;l2;l12]	,  w1								)


let graphStores = takeWhile [Empty] pVar 0 0 graph [] ;;

let graphEval = List.map (evalStore pVar) graphStores ;; *)

(* Start ************************** Test ******************************** *)


let guard1 = St_isNotEq ([y;n], [Null])
let guard4 = St_isNotEq ([x;n], [Null])


let l1 = St_malloc x

let l2 = St_copy (y, x)

let l3 = St_set_n_null ([y;n])

let l4 = St_get_next (y, [y;n])

let l5 = St_malloc t

let l6 = St_get_next (e, [y;n])

let l7 = St_set_next ([t;n], e)

let l8 = St_set_next ([y;n], t)

let l9 = St_set_null y

(* let s1 = mkStore l1 pVar Empty ;; 
let a1 = evalStore pVar s1 ;;

let s2 = mkStore l2 pVar s1 ;;
let a1 = evalStore pVar s2 ;;

let e1  = evalCondition s2 pVar guard1 

(* let s3 = mkStore l4 pVar s2 ;; 
let a1 = evalStore  pVar s3;; *)

let s4 = mkStore l5 pVar s3 ;; 
let a1 = evalStore pVar s4  ;;

let s5 = mkStore l6 pVar s4 ;; 
let a1 = evalStore  pVar s5;;

let s6 = mkStore l7 pVar s5 ;; 
let a1 = evalStore pVar s6 ;;

let s7 = mkStore l8 pVar s6 ;; 
let a1 = evalStore pVar s7 ;; *)


let init = Body ( [l1], Exit );;

(* Start ************************** Functions ******************************** *)

(**** Insert_back start *)
let g1False   	= Body 	( [l5;l6;l7;l8]	,  Exit								)
let g1True 		= Body 	( [l4]  		, (EWH g1False) 					)
let wh_loop		= While ( guard1  		,  g1True 			, g1False		)
let insert 		= Body  ( [l2]			,  wh_loop							)
(**** Insert_back end *)

(**** Pop_front start *)
let g1False1   	= Body 	( []			,  Exit								)
let g1True1 	= Body 	( [l9]  		, (EWH g1False1) 					)
let wh_loop1	= While ( guard1  		,  g1True1 			, g1False1		)
let pop_back 	= Body  ( [l2]			,  wh_loop1							)
(**** Pop_front end *)


(* End *************************** Functions ********************************* *)


let ins0 = call init 0 0 [] pVar [Empty];;
let _malx_ = evalStore pVar (lst ins0)  ;;
let __y_n_  = evalCondition (lst ins0) pVar guard1
let __x_n_  = evalCondition (lst ins0) pVar guard4

(* push *)
let ins1 = call insert 0 0 [] pVar ins0;;
let _push_ = evalStore pVar (lst ins1)  ;;
let __y_n_ = evalCondition (lst ins1) pVar guard1 ;;
let __x_n_ = evalCondition (lst ins1) pVar guard4 ;;

(* push *)
let ins2 = call insert 0 0 [] pVar ins1;; 
let _push_	= evalStore pVar (lst ins2)  ;;
let __y_n_  = evalCondition (lst ins2) pVar guard1 ;;
let __x_n_  = evalCondition (lst ins2) pVar guard4 ;;

(* push *)
let ins3 = call insert 0 0 [] pVar ins2;; 
let _push_	= evalStore pVar (lst ins3)  ;;
let __y_n_  = evalCondition (lst ins3) pVar guard1 ;;
let __x_n_  = evalCondition (lst ins3) pVar guard4 ;;

(* Start ************************** Run ******************************** *)

(* let lc1 = St_isEqual ([x],[y]);;
let lc2 = St_isEqual ([y;n],[t]);;
let lc3 = St_isEqual ([t],[y;n]);;
let lc4 = St_isEqual ([Null],[x]);;
let lc5 = St_isEqual ([x],[Null]);;

let lc6 = St_isNotEq ([x],[y]);;
let lc7 = St_isNotEq ([y;n],[t]);;
let lc8 = St_isNotEq ([t],[y;n]);;
let lc9 = St_isNotEq ([Null],[x]);;
let lc0 = St_isNotEq ([x],[Null]);;

let stl = [l1;l2;l3;l7;l6;l8;l4;l5;l9;l10] ;;
let ctl = [lc1;lc2;lc3;lc4;lc5;lc6;lc7;lc8;lc9;lc0];;

let a = mkStore l1 pVar Empty ;; 
let a1 = evalStore a pVar ;; *)

(* let _____x_eq_y______a = evalCondition a pVar lc1
let ____yn_eq_t______a = evalCondition a pVar lc2
let _____t_eq_yn_____a = evalCondition a pVar lc3
let __null_eq_x______a = evalCondition a pVar lc4
let _____x_eq_null___a = evalCondition a pVar lc5
let _____x_ne_y______a = evalCondition a pVar lc6
let ____yn_ne_t______a = evalCondition a pVar lc7
let _____t_ne_yn_____a = evalCondition a pVar lc8
let __null_ne_x______a = evalCondition a pVar lc9
let _____x_ne_null___a = evalCondition a pVar lc0 *)

(* 
let b = mkStore l2 pVar a ;;
let a1 = evalStore b pVar ;; *)



(* let _____x_eq_y______a = evalCondition b pVar lc1
let ____yn_eq_t______a = evalCondition b pVar lc2
let _____t_eq_yn_____a = evalCondition b pVar lc3
let __null_eq_x______a = evalCondition b pVar lc4
let _____x_eq_null___a = evalCondition b pVar lc5
let _____x_ne_y______a = evalCondition b pVar lc6
let ____yn_ne_t______a = evalCondition b pVar lc7
let _____t_ne_yn_____a = evalCondition b pVar lc8
let __null_ne_x______a = evalCondition b pVar lc9
let _____x_ne_null___a = evalCondition b pVar lc0 *)

(* let c = mkStore l12 pVar b ;; 
let a1 = evalStore c pVar ;;

let g1  = evalCondition c pVar guard1 *)

(* let _____x_eq_y______a = evalCondition c pVar lc1
let ____yn_eq_t______a = evalCondition c pVar lc2
let _____t_eq_yn_____a = evalCondition c pVar lc3
let __null_eq_x______a = evalCondition c pVar lc4
let _____x_eq_null___a = evalCondition c pVar lc5
let _____x_ne_y______a = evalCondition c pVar lc6
let ____yn_ne_t______a = evalCondition c pVar lc7
let _____t_ne_yn_____a = evalCondition c pVar lc8
let __null_ne_x______a = evalCondition c pVar lc9
let _____x_ne_null___a = evalCondition c pVar lc0 *)

(* let d = mkStore l7 pVar c ;;
let a1 = evalStore d pVar ;;

let _____x_eq_y______a = evalCondition d pVar lc1
let ____yn_eq_t______a = evalCondition d pVar lc2
let _____t_eq_yn_____a = evalCondition d pVar lc3
let __null_eq_x______a = evalCondition d pVar lc4
let _____x_eq_null___a = evalCondition d pVar lc5
let _____x_ne_y______a = evalCondition d pVar lc6
let ____yn_ne_t______a = evalCondition d pVar lc7
let _____t_ne_yn_____a = evalCondition d pVar lc8
let __null_ne_x______a = evalCondition d pVar lc9
let _____x_ne_null___a = evalCondition d pVar lc0

let f = mkStore l6 pVar d ;;
let a1 = evalStore f pVar ;;

let _____x_eq_y______a = evalCondition f pVar lc1
let ____yn_eq_t______a = evalCondition f pVar lc2
let _____t_eq_yn_____a = evalCondition f pVar lc3
let __null_eq_x______a = evalCondition f pVar lc4
let _____x_eq_null___a = evalCondition f pVar lc5
let _____x_ne_y______a = evalCondition f pVar lc6
let ____yn_ne_t______a = evalCondition f pVar lc7
let _____t_ne_yn_____a = evalCondition f pVar lc8
let __null_ne_x______a = evalCondition f pVar lc9
let _____x_ne_null___a = evalCondition f pVar lc0

let g = mkStore l8 pVar f ;;
let a1 = evalStore g pVar ;;

let _____x_eq_y______a = evalCondition g pVar lc1
let ____yn_eq_t______a = evalCondition g pVar lc2
let _____t_eq_yn_____a = evalCondition g pVar lc3
let __null_eq_x______a = evalCondition g pVar lc4
let _____x_eq_null___a = evalCondition g pVar lc5
let _____x_ne_y______a = evalCondition g pVar lc6
let ____yn_ne_t______a = evalCondition g pVar lc7
let _____t_ne_yn_____a = evalCondition g pVar lc8
let __null_ne_x______a = evalCondition g pVar lc9
let _____x_ne_null___a = evalCondition g pVar lc0

let h = mkStore l4 pVar g ;;
let a1 = evalStore h pVar ;;

let _____x_eq_y______a = evalCondition h pVar lc1
let ____yn_eq_t______a = evalCondition h pVar lc2
let _____t_eq_yn_____a = evalCondition h pVar lc3
let __null_eq_x______a = evalCondition h pVar lc4
let _____x_eq_null___a = evalCondition h pVar lc5
let _____x_ne_y______a = evalCondition h pVar lc6
let ____yn_ne_t______a = evalCondition h pVar lc7
let _____t_ne_yn_____a = evalCondition h pVar lc8
let __null_ne_x______a = evalCondition h pVar lc9
let _____x_ne_null___a = evalCondition h pVar lc0

let l = mkStore l5 pVar h ;;
let a1 = evalStore l pVar ;;

let _____x_eq_y______a = evalCondition l pVar lc1
let ____yn_eq_t______a = evalCondition l pVar lc2
let _____t_eq_yn_____a = evalCondition l pVar lc3
let __null_eq_x______a = evalCondition l pVar lc4
let _____x_eq_null___a = evalCondition l pVar lc5
let _____x_ne_y______a = evalCondition l pVar lc6
let ____yn_ne_t______a = evalCondition l pVar lc7
let _____t_ne_yn_____a = evalCondition l pVar lc8
let __null_ne_x______a = evalCondition l pVar lc9
let _____x_ne_null___a = evalCondition l pVar lc0

let m = mkStore l9 pVar l ;;
let a1 = evalStore m pVar ;;

let _____x_eq_y______a = evalCondition m pVar lc1
let ____yn_eq_t______a = evalCondition m pVar lc2
let _____t_eq_yn_____a = evalCondition m pVar lc3
let __null_eq_x______a = evalCondition m pVar lc4
let _____x_eq_null___a = evalCondition m pVar lc5
let _____x_ne_y______a = evalCondition m pVar lc6
let ____yn_ne_t______a = evalCondition m pVar lc7
let _____t_ne_yn_____a = evalCondition m pVar lc8
let __null_ne_x______a = evalCondition m pVar lc9
let _____x_ne_null___a = evalCondition m pVar lc0

let o = mkStore l10 pVar m ;;
let a1 = evalStore o pVar ;;

let _____x_eq_y______a = evalCondition o pVar lc1
let ____yn_eq_t______a = evalCondition o pVar lc2
let _____t_eq_yn_____a = evalCondition o pVar lc3
let __null_eq_x______a = evalCondition o pVar lc4
let _____x_eq_null___a = evalCondition o pVar lc5
let _____x_ne_y______a = evalCondition o pVar lc6
let ____yn_ne_t______a = evalCondition o pVar lc7
let _____t_ne_yn_____a = evalCondition o pVar lc8
let __null_ne_x______a = evalCondition o pVar lc9
let _____x_ne_null___a = evalCondition o pVar lc0 *)







(*
let s1 = "St_malloc x" ;;
let a1 = f5 a pVar [(U "u1")] ;;
let a1 = f5 a pVar [(U "u2")] ;;
let s1 = "St_copy (x,y)" ;;
let a1 = f5 b pVar [(U "u1")] ;;
let a1 = f5 b pVar [(U "u2")] ;;
let s1 = "St_malloc e" ;;
let a1 = f5 c pVar [(U "u1")] ;;
let a1 = f5 c pVar [(U "u2")] ;;
let s1 = "St_set_null x" ;;
let a1 = f5 d pVar [(U "u1")] ;;
let a1 = f5 d pVar [(U "u2")] ;;
let s1 = "St_copy (e,x)" ;;
let a1 = f5 f pVar [(U "u1")] ;;
let a1 = f5 f pVar [(U "u2")] ;;
let s1 = "St_copy (t,e)" ;;
let a1 = f5 g pVar [(U "u1")] ;;
let a1 = f5 g pVar [(U "u2")] ;;
let s1 = "St_malloc t" ;;
let a1 = f5 h pVar [(U "u1")] ;;
let a1 = f5 h pVar [(U "u2")] ;;
let a1 = f5 h pVar [(U "u3")] ;;
let s1 = "St_malloc t" ;;
let a1 = f5 l pVar [(U "u1")] ;;
let a1 = f5 l pVar [(U "u2")] ;;
let a1 = f5 l pVar [(U "u3")] ;;
let a1 = f5 l pVar [(U "u4")] ;; 
*)

