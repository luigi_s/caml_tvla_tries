
let loc = "/Volumes/Mac/Users/luigisantivetti/Workspace/OCaml/t1/dot/"

let create nm = open_out (loc ^ nm)

let get file = open_in file

let save file str =
     let channel = create file in
     output_string channel str;
     close_out channel

let rec pred (pVar: string list) (ls : int list) = 
	try 
		match ls with
		| [] -> []
		| [h] -> if h = 0 then [] else [(List.hd pVar)]
		| h::tl -> if h = 0 then pred (List.tl pVar) tl else (List.hd pVar)::(pred (List.tl pVar) tl)
	with 
		Failure _ -> []
