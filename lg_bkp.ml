
type u = U of string | Null

let gI (i:u) = match i with
	| U x -> x
	| Null -> "Null"

(* let ( |??| ) (ls1:u list) (ls2:u list) = 
	let isEqF b i1 i2 = b = ((gI i1) = (gI i2)) in
	(List.fold_left2 isEqF true ls1 ls2)
	
let ( |<>| ) (ls1:u list) (ls2:u list) = 
	let isEqF b i1 i2 = b = ((gI i1) <> (gI i2)) in
	(List.fold_left2 isEqF true ls1 ls2) *)

type v = V

type p = Un of string | Bn of string | Nil

(* let ( |?| ) p1 p2 = 
	let getStr (p:p) = match p with
	| Un x -> x ^ "_u_-_"
	| Bn y -> y ^ "_b_-_"
	| Nil -> "Nil"
	in let compare _p1 _p2 = (getStr _p1) = (getStr _p2)
	in compare p1 p2 *)

let updateUniverse (universe : u list) = 
	let new_u = (U "u_new") in
	match universe with
	| []  							-> [new_u]
	| [h] when [h] |??| [new_u] 	-> [U "u1"]
	| [h] 							-> [U "u2"] @ universe 
	| h::tl when [h] |??| [new_u] 	-> 
		[(U ("u" ^ (string_of_int ((List.length universe)) )))] @ tl
	| ls  							-> 
		[(U ("u" ^ (string_of_int ((List.length universe)+1) )))] @ universe

let updatePVar (pVar : p list) = 
	let isNew = (Un "isNew") in
	match pVar with
	| [] -> []
	| h::tl when h |?| isNew -> tl
	| _ -> [isNew] @ pVar

let mkMatrix ls = 
	let mkDuo x y = [x;y] in
	let rec mtx ls1 ls2 = 
		match ls1 with 
		| [] -> []
		| [h] -> (List.map (mkDuo h) ls2)
		| h::tl -> (List.map (mkDuo h) ls2) @ (mtx tl ls2)
	in mtx ls ls

let mkVector ls = 
	let mkUno x = [x] in 
	List.map mkUno ls

type itp =
	| New 			of (p -> u list -> int)
	| Malloc 		of (p -> u list -> int)
	| Copy 			of (p -> u list -> int)
	| Set_null 		of (p -> u list -> int)
	| Get_next 		of (p -> u list -> int)
	| Set_n_null 	of (p -> u list -> int)
	| Set_next 	 	of (p -> u list -> int)

let applying (i : itp) = match i with
	| New 			i -> i
	| Malloc 		i -> i
	| Copy 			i -> i
	| Set_null 		i -> i
	| Get_next 		i -> i
	| Set_n_null 	i -> i
	| Set_next 	 	i -> i
	
let i_copy (lhs : p) (rhs : p) (i : itp) = 
	let tmp (p1:p) (ls1 : u list) =
		if (p1 = lhs) 
		then (applying i) rhs ls1  
		else (applying i) p1 ls1 in 
	Copy tmp 

let i_isNew =  
	let isNew = (Un "isNew") in
	let u_new = (U "u_new") in
	let tmp (p1:p) (ls1 : u list) =
		if ( (p1 = isNew) && ( ls1 = [u_new])) 
		then 1 else 0 in 
	New tmp 

let i_malloc (lhs : p) = 
	let isNew = (Un "isNew") in
	let copy = i_copy lhs isNew i_isNew in
	Malloc (applying copy)

let i_set_null (lhs : p) (i : itp) = 
	let tmp (p1:p) (ls1 : u list) = 
		if (p1 = lhs) then 0 
		else (applying i) p1 ls1 in
	Set_null tmp 

let i_get_next (lhs : p) (rhs : p list) (i : itp) (universe : u list) = 
	let fsP = List.hd rhs in
	let ndP = List.nth rhs 1 in
	let tmp (p1:p) (ls0 : u list) = 
		let rec inTail (u1:u) (u_ls:u list) = match u_ls with	
		| []    -> 	false
		| [h]   -> 	(((applying i) ndP [u1;h]) = 0) && ([h] = ls0)  
		| h::tl -> 	if (((applying i) ndP [u1;h]) = 0) && ([h] = ls0) 
					then true
					else inTail u1 tl in
		let rec check (ls1 : u list) = match ls1 with
		| []    ->  false
		| [f]   -> 	if ((applying i) fsP [f]) = 0 			then false 
					else if ((applying i) ndP [f;f]) = 0 	then false else true 
		| f::xs -> 	if (((applying i) fsP [f]) = 1) && (((applying i) ndP [f;f]) = 1) 
					then true (* is reflexive *) 
					else if (inTail f xs)				then true else (check xs) 	
		in 	if (p1 <> lhs)
			then (applying i) p1 ls0  
			else if (check universe) then 1 else 0 in 
	Get_next tmp

let i_set_n_null (lhs : p list) (i : itp) = 
	let fsP = List.hd lhs in
	let ndP = List.nth lhs 1 in
	let tmp (p1:p) (ls1 : u list) = 
		if (p1 <> ndP)
		then  ((applying i) p1 ls1)
		else if ((applying i) ndP ls1) = 0 
			then 0 
			else if ((applying i) fsP [(List.hd ls1)]) = 0
				then 1 
				else 0 in 
	Set_n_null tmp

let i_set_next (lhs : p list) (rhs : p ) (i : itp) = 
	let fsP = List.hd lhs in
	let ndP = List.nth lhs 1 in
	let tmp (p1:p) (ls1 : u list) = 
		if (p1 <> ndP)
		then  ((applying i) p1 ls1)
		else if ((applying i) ndP ls1) = 1 
			then 1 
			else if ((applying i) fsP [(List.hd ls1)]) = 0
			then 0 
			else if ((applying i) rhs [(List.nth ls1 1)]) = 0
			then 0 else 1 in
	Set_next tmp


(* Start ************************** Evaluate ******************************** *)

let evaluate_malloc (lhs : p) (pVar : p list) = 
	let forEachPred (arity : u list) (p : p) = 
		(applying (i_malloc lhs)) p arity in 
 	let p_star = List.map (forEachPred [(U "u_new")]) pVar in
 		p_star

let evaluate_copy (lhs : p) ( rhs : p) (pVar : p list) = 
	let forEachPred (arity : u list) ( p : p ) = 
		(applying (i_copy lhs rhs (i_malloc rhs))) p arity in 
	let p_star = List.map (forEachPred [(U "u_new")]) pVar in
		p_star 




(* End **************************** Evaluate ********************************* *)

(* Start *********************** Single evaluate ***************************** *)

let malloc_ (lhs : p) (env : u list) = 
	let p_star = (applying (i_malloc lhs)) lhs [(U "u_new")] in 
	p_star

(* Start *********************** Single evaluate ***************************** *)

let v1 = V;;
let v2 = V;;

let u1 = U "u1";;
let u2 = U "u2";;
let u3 = U "u3";;

let x = Un "x";;
let y = Un "y";;
let t = Un "t";;
let e = Un "e";;
let n = Bn "n";;

let pVar = [x;y;t;e;n] ;;
let universe = [u1;u2;u3];;

let malloc = evaluate_malloc t pVar ;;

let copy = evaluate_copy t x pVar ;;

let malloc1 = malloc_ x [(U "u_new")] ;;
