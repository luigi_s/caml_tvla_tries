# README #

OCaml implementation of static analyser for checking memory safety properties.

In order to run this tool, follow these instructions:

1. Open a terminal in a MacOS environment (not tested on Linux or Windows)
2. Browse into the root directory of the project
3. Type without quotes: “\.valpha.out” 
4. Press [Enter]-key

In order to customise the default behaviour, follow these guidelines:

1. For acquiring information about the memory and then output them use the function:
	callIO (defined at line 898 of the file nls.ml)

2. Do not change from line 1197 up to 1202, the initialisation is the same and useful for any purpose.

3. Set the correct locations where you would like your output to be stored.
(Note that it is not to be one different location for each cfg you are using. It is possible to set the same directory for every location represented at the lines: from 1168 to 1172.
This is because, even with only one shared directory, the system will output ordered filenames for each file it generates)

4. Set the flag defined at line 1165 to the value of “true”

5. Add new call following the pattern provided in the present state of the code. Note:
	This is the structure of one call:
	1. the variable “men” always stores the returning value of callIO
	2. the variable “io” always stores the returning value of doIO
	
	callIO arg1 arg2
	arg1: it is possible to provide any cfg previously defined to the function callIO.
	arg2: it has to be always the variable “men”. It represents the last state in which the memory is.

	doIO arg1 arg2 arg3
	arg1: is the flag for enabling or disabling the IO functionality
	arg2: is the memory, it is always the variable “men”
	arg3: is the directory for the output

Structure of the project:

1. files:
	a. nls.ml, it is the whole source code for executing this tool.
	b. there also all the cfgs used stored into separated files available in the cfgs folder.
	c. *.dot format files, they are the actual output obtained by the analyser. 
	d. *.pdf, they are obtained through the graphviz render engine from the source files encoded in dot.
2. directories:
	a. tests, is the default directory for the output, anytime the IO is on, all the files will be stored there.
	b. examples, it contains renders and dot format files of all the examples we provide together with this project.
	(Note they are borrowed by TVLA which includes them into its latest version available at: 
	http://www.cs.tau.ac.il/~tvla/#Downloads
	c. builds, contains all the blinds compiled at the development stage
	d. cfgs, it contains as separate files the definition of the transition systems implemented

Command used for rendering:
1. It is needed graphviz installed on the system. For more information see:
	http://www.graphviz.org/Download.php
2. once it is installed, all the renders present into every example’s folder are obtained as follow.
	a. open the terminal, only tested under MacOS.
	b. located and browse into the directory containing all the files in dot format
	c. for merging them into only one single pdf file type:
		dot -Tps2 *.dot -o <filename>.ps | ps2pdf <filename>.ps
		(NOTE: <filename> it has to be substituted with the actual plaintext name we want the file to have)


   
	