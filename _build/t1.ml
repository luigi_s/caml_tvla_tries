open Core.Std

(* terms *)
type ('a) term = Var of ('a) | Const of ('a) | Func of ('a) term list

(* predicates *)
type ('a) predicate = Pred of ('a) term list  

(* formulae *)
type ('a) formula =  
  False
| True
| Atomic 	of 'a predicate 
| Not 		of 'a formula
| And 		of 'a formula * 'a formula
| Or 		of 'a formula * 'a formula
| Imp 		of 'a formula * 'a formula
| Iff 		of 'a formula * 'a formula
| Foreach 	of ('a) term * 'a formula
| Exists 	of ('a) term * 'a formula


let x = Var "x";;
let y = Var "y";;

let args = x::y::[];;
args;;

