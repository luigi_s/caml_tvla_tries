type 'a formula =
    False
  | True
  | Atom of 'a
  | Not of 'a formula
  | And of 'a formula * 'a formula
  | Or of 'a formula * 'a formula
  | Imp of 'a formula * 'a formula
  | Iff of 'a formula * 'a formula
  | Forall of string * 'a formula
  | Exists of string * 'a formula
type token =
    TRUE
  | FALSE
  | VAR of string
  | CONST of string
  | FUNC of string
  | PRED of string
  | L_BRACK
  | R_BRACK
  | FORALL
  | EXISTS
  | AND
  | OR
  | IMP
  | IFF
  | NOT
  | COMMA
  | COLON
  | DOT
  | EOF
