
(* working on the unicity obj identity *)

type u = U of int

let gI (i:u) = match i with
	| U x -> x

type no = int
type v = char

type p = Un of no | Bn of no | IsNew | Null

let isBn p = match p with
	| Bn x -> true
	| _ -> false

let gNo (p:p) = match p with
	| Un x -> x
	| Bn x -> x
	| _ -> -1


(* let updateUniverse (universe : u list) = [(U ("u" ^ (string_of_int ((List.length universe)+1) )))] @ universe *)

let updUni (ls : u list) = [( U ((List.length ls)+1) )]@ls

let mkMatrix ls = 
	let mkDuo x y = [x;y] in
	let rec mtx ls1 ls2 = 
		match ls1 with 
		| [] -> []
		| [h] -> (List.map (mkDuo h) ls2)
		| h::tl -> (List.map (mkDuo h) ls2) @ (mtx tl ls2)
	in mtx ls ls

let fvMatrix ls v = 
	let mkDuo x y = 
		match v with
		 	| [] -> []
		 	| [h] -> [h;y] 	
		 	| h::tl -> [h;y]
		in List.map (mkDuo v) ls

let fvMatrixL ls v = 
	let mkDuo x y = 
		match v with
		 	| [] -> []
		 	| [h] -> [y;h] 	
		 	| h::tl -> [y;h]
		in List.map (mkDuo v) ls

let mkVector ls = 
	let mkUno x = [x] in 
	List.map mkUno ls

type statement = 
	| St_malloc 	of p ref  
	| St_set_null 	of p ref 
	| St_copy 		of p ref * p ref  
	| St_get_next 	of p ref * p ref list
	| St_set_next 	of p ref list * p ref
	| St_set_n_null of p ref list


let isMal s = match !s with
	| St_malloc x -> true	
	| _ -> false

let getLhs st = 
	match !st with
	| St_malloc 	 lhs 		-> [lhs]
	| St_set_null 	 lhs 		-> [lhs]
	| St_copy 		(lhs, rhs) 	-> [lhs] 
	| St_get_next 	(lhs, rhs) 	-> [lhs]
	| St_set_next 	(lhs, rhs) 	->  lhs
	| St_set_n_null  lhs 		->  lhs

let getRhs st = 
	match !st with
	| St_copy 		(lhs, rhs) 	-> [rhs] 
	| St_get_next 	(lhs, rhs) 	->  rhs
	| St_set_next 	(lhs, rhs) 	-> [rhs] 
	| _ 						-> []

type itp =
	| New 			of (p -> u list -> int)
	| Malloc 		of (p -> u list -> int)
	| Copy 			of (p -> u list -> int)
	| Set_null 		of (p -> u list -> int)
	| Get_next 		of (p -> u list -> int)
	| Set_n_null 	of (p -> u list -> int)
	| Set_next 	 	of (p -> u list -> int)
	| Init 			

let applying (i : itp) = match i with
	| New 			i -> i
	| Malloc 		i -> i
	| Copy 			i -> i
	| Set_null 		i -> i
	| Get_next 		i -> i
	| Set_n_null 	i -> i
	| Set_next 	 	i -> i
	| _ 			  -> (fun p env -> 0) 

type formula = 
	| Atom of p ref * v list (* arity k *)
	| Not of formula
	| Eq of formula * formula
	| And of formula * formula
	| Or of formula * formula
	| Imp of formula * formula
	| Exists of v list * formula
	| Forall of v list * formula
	| False 

(* Start ************************ Structure ******************************** *)

type structure = S of p ref * u list ref * itp | Nothing 

let predOf s = match s with
	| S 	( p , _ , _ ) -> p 
	| Nothing -> ref Null

let itpOf s = match s with
	| S 	( _ , _ , i ) -> i 
	| Nothing -> Init

let universeOf s = match s with
	| S 	( _ , u , _ ) -> u 
	| Nothing -> ref []

(* End ************************ Structure ******************************** *)


(* Start ************************ Store ******************************** *)

type store = Store of (structure list * u list)

let rec sUniverse s = let Store ( _ , u ) = !s in u

let sStruct p store = 
	let Store ( sl , _ ) = !store
	in let rec getOne sl1 = match sl1 with
	| [] -> Nothing
	| [h] -> if (predOf h) = p then h else Nothing
	| h::tl -> if (predOf h) = p then h else getOne tl
in getOne sl

let sItp p store = applying (itpOf (sStruct p store)) !p

let setUni s uni = match s with
	| S  ( x , u , z ) -> S (x , uni, z )
	| _ -> s

(* End ************************ Store ******************************** *)

(* Start ************************ Formulae ******************************** *)

(*
	Algorithm for checking one by one (without the need to generate all the possible enviroments in one go)
	every possible assignment of individuals for each binary predicate's variable appearing throughout a formula.

	example on a given universe:
	[ u4 , u3 , u2 , u1 ]
	
	step one, re-arrange for assuring termination
	4 3 2 1 -> into -> 3 2 1 4 3

	3 2 1 4	3 			fs -> where f is first and s second
						32 <- [(v1, u3);(v2, u2)]
	3 1 4 	3 2 
						31 <- [(v1, u3);(v2, u1)]	
	3 4 	3 2 1
						34 <-  ...
	3 		3 2 1 4  
						33 
	2 1 4 3 2
						21
	2 4 3   2 1
						24
	2 3     2 1 4
						23
	2       2 1 4 3
						22
	1 4 3 2 1
						14
	1 3 2   1 4
						13
	1 2     1 4 3
						12
	1       1 4 3 2
						11
	4 3 2 1 4
						43
	4 2 1   4 3
						42
	4 1     4 3 2
						41
	4       4 3 2 1
						44
*)

(* 
	verifying sat of unary predicates. 
	Treating separately unary and binary pred. allows to speed the process up
	It exploits the property that pointers can address one and only one location at the time.

	takes	: store -> variables list -> predicate
	returns	: (v * u) list  
 *)
let satUn uni itp p1 = 
	let isSat env = (itp p1 env) = 1 in
	let zL u1 ls = (isSat [u1]) || ls in
	let rec cmb uni1 = 
		match uni1 with
		| [] 		-> 	false					
		| [h] 		-> 	zL h false
		| h::tl 	-> 	zL h (cmb tl)
	in cmb !uni

(* 
	implements the procedure presented above for exists quantifier.

	takes	: store -> variables list -> predicate -> u list ref
	returns	: (v * u) list  
 *)
let satBn univ itp p1 = 
	let stop = (List.hd !univ) in
	let isSat env = (itp p1 env) = 1 in
	let zL u1 u2 ls = (isSat [u1;u2]) || ls 
	in let rec cmb uni1 = 
		match uni1 with
		| [] 		-> 	false
		| [h] 		-> 	zL h h false 
		| h::s::[] 	-> 	zL h h (cmb [s]) 
		| h::s::tl 	-> 	if ( h = s ) && ( h = stop )
						then zL h s (cmb [s])
						else if ( h = s )
							 then zL h s ( cmb (tl@[h]@[(List.hd tl)]) ) 
							 else zL h s ( cmb ((h::tl)@[s]) 		   ) 
		in			
		let prepare uni2 = match !uni2 with
			| [] 		-> []
			| [h] 		-> [h]
			| h::s::[] 	-> s::h::s::[]
			| h::s::tl 	-> s::tl@[h]@[s]
			in let setup = prepare univ in
		cmb setup 

(* 
	implements the procedure presented above for forall quantifier.

	it genertes over a universe every different call a different enviroment to be checked
	takes	: (u list * ulist) and a stop value u
	returns	: u list  
 *)
let gen (t_uni : u list * u list) (stop : u) = 
	let uni = (snd t_uni) in
	let res = (fst t_uni) in
	let zL u1 u2 ls = ([u1;u2], ls) in 
	let rec cmb uni1 = 
		match uni1 with
		| [] 		->  ([],[])
		| [h] 		-> 	(zL h h [])
		| h::s::[] 	-> 	(zL h h [s]) 
		| h::s::tl 	-> 	if ( h = s ) && ( h = stop )
						then ( zL h s [] )
						else if ( h = s )
							 then ( zL h s (tl@[h]@[(List.hd tl)])    ) 
							 else ( zL h s ((h::tl)@[s])   )  
		in			
		let prepare uni2 = match uni2 with
			| [] 		-> []
			| [h] 		-> [h]
			| h::s::[] 	-> s::h::s::[]
			| h::s::tl 	-> s::tl@[h]@[s]
			in 
			if res = [] then cmb (prepare uni) else cmb uni 		 

(* 
	to be called on any predicates (unary or binary) 
 *)
let sat uni itp p1 = match p1 with
	| Bn x 	-> (satBn uni itp p1)
	| Un x 	-> (satUn uni itp p1)
	| _ -> let msg = Printf.printf ("ERROR: Unmatched predicate for sat \n ") in msg; false


(* 
	Exists quantifier. It extracts from the tuples' lists those variables the are bound and checks for all bound variables
	if they exist in the resulset that satisfies the formula

	i.e.

	given the formula:

	f: Ex | P(x) /\ Q(x,y) 

	given s1 < U, i >

	it found that for z:
	
	z: [(x->u1);(y->u2)]
	
	exists checks if x is assigned in z 
*)
(* let exists vl res = 
	let f3 v t = (fst t) = v in
	let f5 res v1 = List.exists (f3 v1) res in 
	List.for_all (f5 res) vl *)

(*

vl1 : ['x', 'y', 'z']
ul  : [u1 , u2 , u3 ]

p   : ['x', 'z']

*)
let rec z vl1 ul vl2 = 
	let ct ls = match ls with
	| [] -> []
	| [h] -> [h]
	| h::tl -> tl in
	let rec f1 vl1 ul v =
		match vl1 with
		| []  -> []
		| [h] -> if (h = v) then [(List.hd ul)] else [] 
		| h::tl -> if (h = v) then (List.hd ul)::(f1 tl (ct ul) v) else (f1 tl (ct ul) v)
		in
		match vl2 with
		| [] -> []
		| [h] -> (f1 vl1 ul h)
		| h::tl -> (f1 vl1 ul h)@(z vl1 ul tl)

(* z assignment must be complete *)
let check p zs =
	let ( z , s ) = zs in 
	let isSat env = ((sItp p s) env) = 1 in
	isSat z

(* it checks recursively if there exists an enviroment that satisfies the formula with bound variables
	without generating all the possible cases prior, but returning false only when the tries are finished.
	Quits with true otherwise *)
let exists stop uni f z1 = 
	let rec call res = 
		if (snd res) = [] 
		then false
		else 	let p_res = (gen res stop) in
				(f ( z1 (fst p_res) )) || (call (gen p_res stop)) 
	in call ( [] , uni )

(* it checks recursively if there exists an enviroment that satisfies the formula with bound variables
	without generating all the possible cases prior, but returning false only when the tries are finished.
	Quits with true otherwise *)
let forall stop uni f z1 = 
	let rec call res = 
		if (snd res) = [] 
		then true
		else 	let p_res = (gen res stop) in
				(f ( z1 (fst p_res) )) && (call (gen p_res stop)) 
	in call ( [] , uni )

(* 
	it takes two assignments, one for quantified variable and one for free ones.
	it appends in the right order the individuals for forming an environment to be checked
*)
let sw f1 f2 (ls : v list) =
	match ls with
	| [] -> []
	| [h] -> (f1 [h])@(f2 [h])
	| h::s::[] -> (f1 [h])@(f2 [h])@(f1 [s])@(f2 [s])
	| _ ->  let msg = Printf.printf ("ERROR: sw ul, only unary and binary predicates supported yet \n ") in msg;[]


(* 
	recursively calls the sat procedure on every branch of a formula parse tree

	takes :		(v list -> u list) * store -> formula 
	returns : 	bool
 *)
let ( |- ) zs formula =
	let ( agn , store ) = zs in
	let quni = sUniverse store in
	let stop = List.hd quni in
	let rec eVAL formula vlq = 
		match formula with
		| Atom 		( p , vl) 		->  check p ( (sw agn vlq vl) , store ) 

		| Not 		 fi  			->  not (eVAL fi vlq)

		| And 		(fi1,fi2)		->  (eVAL fi1 vlq) && (eVAL fi2 vlq ) 

		| Or 		(fi1,fi2)		-> 	(eVAL fi1 vlq) || (eVAL fi2 vlq)  

		| Imp 		(fi1,fi2)		-> 	(not(eVAL fi1 vlq)) || (eVAL fi2 vlq) 

		| Eq 		(fi1,fi2)		-> 	(eVAL fi1 vlq ) = (eVAL fi2 vlq) 

		| Exists 	( vl,fi ) 		-> 	let z1 = z vl in ( exists stop quni (eVAL fi) z1 )

		| Forall 	( vl, fi)		->  let z1 = z vl in ( forall stop quni (eVAL fi) z1 )

		| _ -> let msg = Printf.printf ("ERROR: Unmatched formula in eval \n ") in msg;false
	in eVAL formula (z [] [])

(* End ************************** Formulae ******************************** *)

(* Start ************************** Conditions ******************************** *)

let isEQ x y s =
	let a1 = Atom ( x , ['x'] ) in 
	let a2 = Atom ( y , ['x'] ) in
	let imp1 = Imp ( a1, a2 ) in
	let imp2 = Imp ( a2, a1 ) in
	let con = And ( imp1 , imp2 ) in
	let formula = Forall ( ['x'], con) in
	(( z [] []) , s) |- formula

let isNE x y s =
	let a1 = Atom ( x , ['x'] ) in 
	let a2 = Atom ( y , ['x'] ) in
	let imp1 = Imp ( a1, a2 ) in
	let imp2 = Imp ( a2, a1 ) in 
	let con = And ( imp1 , imp2 ) in
	let n = Not ( con ) in
	let formula = Exists ( ['x'], n) in
	(( z [] []) , s) |- formula

let isNU x s =
	let a1 = Not (Atom ( x , ['x'] )) in 
	let formula = Forall ( ['x'], a1 ) in
	(( z [] []) , s) |- formula

let isNN x s = 
	let a1 = (Atom ( x , ['x'] )) in 
	let formula = Exists ( ['x'], a1 ) in
	(( z [] []) , s) |- formula

(* End ************************** Conditions ******************************** *)

(* Start ************************** Actions ******************************** *)

let n_isNew ( s : store ref ) =
	let tmp ( p : p ) (ul : u list) = 
	if (p = IsNew) && ul = [(List.hd (updUni (sUniverse s)))] then 1 else 0
in New tmp

let n_malloc ( lhs : p ref ) ( s : store ref ) = 
	let tmp ( p : p ) ( ul : u list ) =
		if (p = !lhs) 
		then
			let z1 = z ['x'] ul in
			if (z1 , s) |- (Atom ((ref IsNew) , ['x'])) then 1 else 0
		else match p with
		| Un x  -> 	let z1 = z ['x'] ul in
					if (z1 , s) |- (And ( (Atom ( (ref p) , ['x'] )) , (Not ( (Atom ( (ref IsNew) , ['x'] )))))) then 1 else 0
		| Bn x 	-> 	let z1 = z ['x';'y'] ul in 
					let n_xy = Atom ( (ref p) , ['x';'y'] ) in
				  	let not_isNew_x = Not ( (Atom ( (ref IsNew) , ['x'] ))) in
				  	let not_isNew_y = Not ( (Atom ( (ref IsNew) , ['y'] ))) in
				  	if (z1 , s) |- ( And ( n_xy , (And ( not_isNew_x , not_isNew_y )))) then 1 else 0 	  	
		| _ 	-> 	let msg = Printf.printf ("ERROR: n_malloc not reachable \n ") in msg;0
	in Malloc tmp

let n_copy (lhs : p ref) (rhs : p ref ) (s : store ref) = 
	let tmp ( p : p ) ( ul : u list ) =
		let z1 = z ['x'] ul in 
		if (p = !lhs) 
		then if ((z1 , s) |- (Atom ( rhs      , ['x'] ))) then 1 else 0
		else if ((z1 , s) |- (Atom ( (ref p)  , ['x'] ))) then 1 else 0
	in Copy tmp 

let n_get_next (lhs : p ref) (rhs : p ref list) (s : store ref ) = 
	let tmp (p : p) ( ul : u list ) = 
		if (p = !lhs) 
		then
			let z1 = z ['y'] ul in
			let rp1 = List.hd rhs in
			let rp2 = List.nth rhs 1 in
			let un = Atom ( rp1 , ['x'] ) in 
			let bn = Atom ( rp2 , ['x';'y']) in 
			if (z1 , s) |- (Exists ( ['x'] , (And (un , bn )) )) then 1 else 0
		else match p with
			| Un x  -> 	let z1 = z ['x'] ul in
						if (z1 , s) |- (Atom ( (ref p) , ['x'] ))  then 1 else 0
			| Bn x 	-> 	let z1 = z ['x';'y'] ul in 
					  	if (z1 , s) |- (Atom ( (ref p) , ['x';'y'] ))  then 1 else 0 	  	
			| _ 	-> 	let msg = Printf.printf ("ERROR: n_get_next not reachable \n ") in msg;0
	in Get_next tmp

let n_set_null (lhs : p ref) (s : store ref) = 
	let tmp (p : p) ( ul : u list) = 
		if (p = !lhs) then 0 
		else match p with
			| Un x  -> 	let z1 = z ['x'] ul in
						if (z1 , s) |- (Atom ( (ref p) , ['x'] ))  then 1 else 0
			| Bn x 	-> 	let z1 = z ['x';'y'] ul in 
					  	if (z1 , s) |- (Atom ( (ref p) , ['x';'y'] ))  then 1 else 0 	  	
			| _ 	-> 	let msg = Printf.printf ("ERROR: n_get_next not reachable \n ") in msg;0
	in Set_null tmp 

let n_set_next ( lhs : p ref list ) (rhs : p ref) (s :store ref) = 
	let tmp (p : p) ( ul : u list) = 
	let lp1 = List.hd lhs in 
	let lp2 = List.nth lhs 1 in 
	if (p = !lp2) then
		let z1 = z ['x';'y'] ul in 
		let bn = Atom ( (ref p) , ['x';'y'] ) in 
		let un1 = Atom ( lp1 , ['x']) in
		let un2 = Atom ( rhs , ['y']) in 
		if (z1 , s) |- (Or ( bn , (And ( un1 , un2 )))) then 1 else 0
	else match p with
			| Un x  -> 	let z1 = z ['x'] ul in
						if (z1 , s) |- (Atom ( (ref p) , ['x'] ))  then 1 else 0
			| Bn x 	-> 	let z1 = z ['x';'y'] ul in 
					  	if (z1 , s) |- (Atom ( (ref p) , ['x';'y'] ))  then 1 else 0 	  	
			| _ 	-> 	let msg = Printf.printf ("ERROR: n_get_next not reachable \n ") in msg;0
	in Set_next tmp

let n_set_n_null (lhs : p ref list) (s : store ref) = 
	let tmp ( p : p) ( ul : u list ) = 
	let lp1 = List.hd lhs in
	let lp2 = List.nth lhs 1 in 
	if (p = !lp2) then
		let z1 = z ['x';'y'] ul in 
		let bn = Atom ( lp2, ['x';'y'] ) in
		let un = Not (Atom ( lp1, ['x'])) in
		if (z1 , s) |- (And ( bn , un )) then 1 else 0
	else match p with
			| Un x  -> 	let z1 = z ['x'] ul in
						if (z1 , s) |- (Atom ( (ref p) , ['x'] ))  then 1 else 0
			| Bn x 	-> 	let z1 = z ['x';'y'] ul in 
					  	if (z1 , s) |- (Atom ( (ref p) , ['x';'y'] ))  then 1 else 0 	  	
			| _ 	-> 	let msg = Printf.printf ("ERROR: n_get_next not reachable \n ") in msg;0
	in Set_n_null tmp

(* End ************************** Actions ******************************** *)

(* it creates n empty structure for the begin *)
let initStore pVar = 
	let rec init ls = 
	match ls with
	| [] -> []
	| [h] -> [(S ( ref h , ref [] , Init ))]
	| h::tl -> ( (S ( ref h , ref [] , Init ))) :: (init tl)
	in Store ( (init pVar) , [] ) 

(* it creates a set of n (one for each p) structures for each statement *)
let mkStore (stmt : statement ref ) (input : store ref) = 
	let oldUni = (sUniverse input) in (*ref*) 
	let newUni = if isMal stmt then updUni oldUni else oldUni in (*copy*)
	let actionOn stM stO p uni = match !stM with
	| St_malloc x   	-> 	let newS = Store ( [( S ( (ref IsNew) , (ref oldUni) ,  (n_isNew stO)) )] , !uni ) in
							if x = p
							then ( S ( x , uni , (n_malloc x (ref newS) ) ) )
							else (setUni (sStruct p stO) uni)	

	| St_copy (x,y) 	-> 	if x = p 
							then ( S ( x , uni , (n_copy x y stO) ) ) 
							else (setUni (sStruct p stO) uni)

	| St_set_null x 	-> 	if x = p 
							then ( S ( x , uni , (n_set_null x stO) ) ) 
							else (setUni (sStruct p stO) uni)

	| St_get_next (x,y) ->  if x = p 
							then ( S ( x , uni , (n_get_next x y stO) ) ) 
							else (setUni (sStruct p stO) uni)

	| St_set_next (x,y) ->  let lp2 =  (List.nth x 1) in
							if lp2 = p
							then ( S ( lp2 , uni , (n_set_next x y stO) ) ) 
							else (setUni 		(sStruct p stO) uni)	

	| St_set_n_null x 	->	let lp2 =  (List.nth x 1) in
							if lp2 = p 
							then ( S ( lp2 , uni , (n_set_n_null x stO) ) ) 
	 						else (setUni (sStruct p stO) uni)
	in let getStructures s = let Store ( sl , _ ) = !s in sl 
		in let rec forEachStruct sl1 =  
			match sl1 with
			| [] 	-> 	[]
			| [h] 	-> 	[(actionOn stmt input (predOf h) (ref newUni))]
			| h::tl -> 	(actionOn stmt input (predOf h) (ref newUni)) :: (forEachStruct tl)
			in Store ( (forEachStruct (getStructures input)) , newUni)

let rec mkStores (stl : statement ref list ) (s : store ref) = 
	match stl with
	| [] -> []
	| [h] -> [ref (mkStore h s)]
	| h::tl -> let newStore = ref (mkStore h s) in
(mkStores tl newStore)@[newStore]

(* Start ************************** Evaluate store ********************************* *)

let evalStruct pVar s = 
	let onStruct st = 
		let uni = universeOf st in
		let itp = applying (itpOf st) in
		List.map (sat uni itp) pVar
	in let getStructures s1 = let Store ( sl , _ ) = !s1 in sl 
		in let rec forEachStruct sl1 =  
			match sl1 with
			| [] 	-> 	[]
			| [h] 	-> 	[(onStruct h)]
			| h::tl -> 	(onStruct h) :: (forEachStruct tl )
			in forEachStruct (getStructures s)

let eval store = 
	let Store ( lst , u ) = !store in 
	let mp p = match p with
	| Un x -> mkVector u
	| Bn x -> mkMatrix u
	| _ -> []
	in
	let rec ovr ls = 
	match ls with
	| [] 	-> []
	| [h] 	-> [ (List.map (applying (itpOf h) !(predOf h)) (mp !(predOf h))) ]
	| h::tl -> 	(List.map (applying (itpOf h) !(predOf h)) (mp !(predOf h)))::(ovr tl)
	in ovr lst

let eval1 pVar sel store = 
	let Store ( ls1 , u ) = !store in
	let ownUn sl pl = 
		let rec getItps ls = match ls with
		| [] -> []
		| [h] -> if (isBn !(predOf h)) then [] else [(itpOf h)]
		| h::tl -> if (isBn !(predOf h)) then (getItps tl) else (itpOf h)::(getItps tl)
		in let itps = getItps sl in  
		List.map2 applying itps pl in
	let ownBn sl pl = 
		let rec getItps ls = match ls with
		| [] -> []
		| [h] -> if not (isBn !(predOf h)) then [] else [(itpOf h)]
		| h::tl -> if not (isBn !(predOf h)) then (getItps tl) else (itpOf h)::(getItps tl)
		in let itps = getItps sl in 
		List.map2 applying itps pl in 
	let ovr ls = match ls with
	| [] 	-> ([], [])
	| [h] 	as un -> ((List.map ((|>) un ) (ownUn ls1 pVar)), un)
	| h::tl as bn -> ((List.map ((|>) bn ) (ownBn ls1 sel)),bn)
	in (List.map ovr ((mkVector u)@(mkMatrix u)))

(* End **************************** Evaluate store ********************************* *)

(* Start ************************** Flow Control ******************************** *)

type node = 
	| PRC of (int * ( node list ref ) * ( node list ref ) * statement ref list) 
	| CRT of (int * ( node list ref ) * ( node list ref ) * (store ref -> bool) ) 
	| ERROR
	| END

let nodeId n = match n with
	| PRC (x,_,_,_) -> x
	| CRT (x,_,_,_) -> x
	| _ -> 0

let isPrc n = match n with
	| PRC (_,_,_,_) -> true
	| _ -> false

let isCrt n = match n with
	| PRC (_,_,_,_) -> false
	| _ -> true

let guard g = match g with
	| CRT (_,_,_,guard) -> guard
	| _ -> let msg = Printf.printf ("ERROR: node not CRT \n ") in msg; ( fun s -> false )

let outgoing g = 
	let ( _ , nl ) = g in
	let mN n = match n with 
			| PRC (_,ol,_,_) -> !ol
			| CRT (_,ol,_,_) -> !ol
			| _ -> []
		in
	let rec out ls =
	match !ls with
	| [] -> []
	| [h] -> mN h
	| h::tl ->  (mN h)@(out (ref tl))
	in out nl

let add_control ( g , f ) =
   let newId ( c, nl ) = 
	    incr c;  
	   	let ctr = CRT (!c, ref [] , ref [] , f ) in
	   	nl := ctr :: !nl; ctr
	in newId g 

let add_process ( g , stl ) = 
	let newId (c , nl) = 
		incr c;
		let prc = PRC (!c, ref [] , ref [] , stl) in 
		nl := prc :: !nl; prc
	in newId g

type edge = node * node

let validEdge a b = 
	match a with
	| PRC (_,_,_,_) -> not (isPrc b)
	| ERROR -> false
	| _ -> true

let add_edge ( a , b ) = 
	if validEdge a b
	then
		let fromAny node = match node with
		| PRC (_,ol,_,_) 	-> ol
		| CRT (_,ol,_,_) 	-> ol
		| _ -> ref []
		in
		let toAny node = match node with
		| PRC (_,_,il,_) -> il
		| CRT (_,_,il,_) -> il
		| _ -> ref []
		in 
		let ol,il = fromAny a, toAny b in 
		ol := b :: !ol;
		il := a :: !il
	else Printf.printf ("\nERROR: Invalid edge, from nID %d to nID %d\n\n") (nodeId a) (nodeId b)

type graph = (int ref * (node list) ref)

let create_graph () = (ref 0, ref [])

let getNodes g = let ( _ , nl ) = g in nl

let getFst g = 
	let ( _ , nl ) = g in 
	let rec lst ls = 
	match !ls with
		| [] -> ERROR
		| [h] -> h 
		| h::tl -> lst (ref tl)
	in lst nl

let call g s = 
	let fs = ref (getFst g) in 
	let rec get n ns = 
	match !n with
	| PRC (_,ol,_, sl ) -> 	let stores = mkStores sl ns   in
							let nx_s = (List.hd stores)   in 
							let nx_n = ref (List.hd !ol)  in
							( get nx_n nx_s )@(stores)
	| CRT (_,ol,_, co ) -> 	let tb = ref (List.nth !ol 1) in
							let fb = ref (List.hd !ol) 	  in
							if co ns then get tb ns else get fb ns
	| END 				->  []
	| _ -> []
	in get fs s

(* End **************************** Flow control ********************************* *)

(* Start ************************** Test ******************************** *)

let x = (Un 1);;
let y = (Un 2);;
let t = (Un 3);;
let e = (Un 4);;
let n = (Bn 5);;
let d = (Un 6);;

let pVar_ = [ "x";"y";"t";"e";"d"];;
let sel_  = [ "n" ] ;;

let pVar = [  x; y; t; e; d] ;;
let sel  = [ n ] ;;

let l1 	= St_malloc (ref x);;
let l2a = St_get_next ( (ref d), [(ref x);(ref n)] );;
let l2 	= St_copy ((ref y), (ref x));;
let l3 	= St_set_n_null ([(ref y);(ref n)]);;
let l4 	= St_get_next ((ref y), [(ref y);(ref n)]);;
let l4a = St_get_next ((ref d), [(ref d);(ref n)]);;
let l5 	= St_malloc (ref t) ;;
let l6 	= St_get_next ((ref e), [(ref y);(ref n)]);;
let l7 	= St_set_next ([(ref t);(ref n)], (ref e));;
let l9 	= St_set_next ([(ref y);(ref n)], (ref t));;

let l10 = St_set_null (ref y);;

let msg = Printf.printf ("\n \t***\t***\tStore \n\n ") in msg;;
(* CFG for insert --- START --- *)
let insert = create_graph () ;;

let node1 = add_process ( insert , [(ref l2a); (ref l2)]) ;;
let node2 = add_control ( insert , ( isNN (ref d))) ;;
let node3 = add_process ( insert , [(ref l4);(ref l4a)]) ;;
let node4 = add_process ( insert , [(ref l5); (ref l6); (ref l7); (ref l9)] ) ;;

let edge1 = add_edge ( node1 , node2 ) ;;
let edge2 = add_edge ( node2 , node3 ) ;;
let edge3 = add_edge ( node3 , node2 ) ;;
let edge4 = add_edge ( node2 , node4 ) ;;
let edge5 = add_edge ( node4 , END   ) ;;
(* CFG for insert --- END --- *)

(* init heap at empty *)
let empty = initStore (pVar@sel);;

(* allocates the first pointer (due to the assertion x!=NULL) *)
let main = mkStore (ref l1) (ref empty);;

(* makes first call insert *)
let heap1 = call insert (ref main);;

(* numerical representation of the heap (it should be a dot file) *)
(* let e1 = List.map eval heap1 ;; *)
let e1b = List.map (eval1 pVar sel) heap1 ;;

(* resets the automatic variables *)
let heap2 = mkStores [ (ref (St_set_null (ref y)));(ref (St_set_null (ref t)));(ref (St_set_null (ref e)));(ref (St_set_null (ref d)))] (List.hd heap1) ;;

let e2b = List.map (eval1 pVar sel) heap2 ;;

(* makes second call insert *)
let heap3 = call insert (List.hd heap2);;

let e3b = List.map (eval1 pVar sel) heap3 ;;


(* numerical representation of the heap (it should be a dot file) *)
(* let e1 = List.map eval heap2 ;; *)

(* resets the automatic variables *)
let resetStack = mkStores [ (ref (St_set_null (ref y)));(ref (St_set_null (ref t)));(ref (St_set_null (ref e)));(ref (St_set_null (ref d)))] (List.hd heap2) ;;

(* End ************************** Test ******************************** *)

(* Start ************************** I/O ******************************** *)
let create nm = open_out nm

let get file = open_in file

let save file str =
     let channel = create file in
     output_string channel str;
     close_out channel;;

let stmt_to_string st = match st with
	| St_malloc 	x  		-> "Malloc"
	| St_set_null 	x  		-> "Set_null"
	| St_copy 		(x,y)  	-> "Copy"
	| St_get_next 	(x,yl) 	-> "Get_next"
	| St_set_next 	(xl,y)	-> "Set_next"
	| St_set_n_null (xl)	-> "Set_next_null"

let pred (pVar: string list) (sel : string list) t = 
	try 
		let ( li , env ) = t in
		let rec onUn ls preds = 
		match ls with
		| [] -> ""
		| [h] -> if h = 0 then "" 
					else (Printf.sprintf ("\t%s [margin=0 fontcolor=black fontsize=16 shape=plaintext]\n\t%s -> u%s;\n") (List.hd preds) (List.hd preds) (string_of_int (gI (List.hd env))))
		| h::tl -> if h = 0 then onUn tl (List.tl preds) 
					else (onUn tl (List.tl preds))^(Printf.sprintf ("\t%s [margin=0 fontcolor=black fontsize=16 shape=plaintext]\n\t%s -> u%s;\n") (List.hd preds) (List.hd preds) (string_of_int (gI (List.hd env))))
		in let rec onBn (ls : int list) preds = 
		match ls with
		| [] -> ""
		| [h] -> if h = 0 then "" 
					else (Printf.sprintf ("\tu%s -> u%s [ label = \"%s\" ];\n") (string_of_int (gI (List.hd env))) (string_of_int (gI (List.nth env 1))) (List.hd preds))
		| h::tl -> if h = 0 then onBn tl (List.tl preds) 
					else (onBn tl (List.tl preds))^((Printf.sprintf ("\tu%s -> u%s [ label = \" %s \" ];\n") (string_of_int (gI (List.hd env))) (string_of_int (gI (List.nth env 1))) (List.hd preds)))
		in let uNBn ls = match env with
		| [h] 		-> onUn ls pVar
		| h::s::[] 	-> onBn ls sel
		| _ -> ""
		in (uNBn li)
	with 
		Failure _ -> "\nERROR in generation .dot file\n"

(* labelloc=t;\n\t *)
(* 


 
 *)

let mkDot sl el num =
	(* try *) 
		let loc = "/Volumes/Mac/Users/luigisantivetti/Workspace/OCaml/t1/dot/" in
		let stl = sl in
		let e = List.rev el in
		let header = "digraph G\n{\n" in
		let footer = "\n}\n\n" in
		let node = "\tnode [fontcolor=black fontsize=16 shape=circle]\n\t" in 
		let layout = "rankdir=\"LR\";\n\torientation=landscape;\n\t" in
		let size = "size=\"20,10\"\n\t" in
		let title st = Printf.sprintf ("\tlabel=\"%s\";\n\tlabeljust=left;\n\tfontsize=20;\n") (stmt_to_string st) in
		let i n = Printf.sprintf ("%05d") n in
		let rec mkFile ls st n = match ls with
		| [] 		-> ()
		| [h] 		->	let msg = Printf.printf ("\nh::s\n %s \n") (stmt_to_string st) in msg;
						let t = (n^"_")^(stmt_to_string st) in save (loc^t^".dot") (header^(title st)^size^node^layout^h^footer)
		| h::s::[] 	-> 	mkFile [(h^s)] st n
		| h::s::tl 	-> 	mkFile ([(h^s)]@tl) st n in
		let oneStore = List.map (pred pVar_ sel_ ) in
		let rec ovr ls lst n = 
		match ls with
		| [] 		-> []
		| [h] 		-> [(mkFile (oneStore h) (List.hd lst) (i n))]
		| h::tl 	->  (mkFile (oneStore h) (List.hd lst) (i n))::(ovr tl (List.tl lst) (n+1) )
		in ovr e stl num
	(* with
		Failure _ -> Printf.printf ("\nERROR: mkDot uncaught \n\n"); [] *)


let stml1 = [(l1);(l2);(l5);(l6);(l7);(l9)];;
let aaa1 = mkDot stml1 e1b 1 ;;

let stml2 = [(l10);(l10);(l10);(l10)];;
let aaa2 = mkDot stml2 e2b 7 ;;

let stml3 = [(l2);(l2);(l4);(l4a);(l5);(l6);(l7);(l9)];;
let aaa3 = mkDot stml3 e3b 11 ;;




