(* STILL MISSING matching for binary predicates *)
let rec z_agn pr univ = 
	let universe = indFromUni univ in 
	match pr with
	| UP x when (List.length universe) > 1 -> (UP (List.hd universe)) :: z_agn pr (Uni (List.tl universe))
	| UP x when (List.length universe) = 1 -> (UP (List.hd universe)) :: z_agn pr (Uni [])
	| UP x when (List.length universe) < 1 -> []
	| _ -> []

let ( <_> ) i1 i2 = 
	let toBool (x, y) = match (x,y) with 
	| (Ind v1, Ind v2) when v1 <> v2 -> true
	| (Ind v1, Ind v2) -> false
	in toBool (i1, i2)

let ( =_= ) i1 i2 = 
	let toBool (x, y) = match (x,y) with 
	| (Ind v1, Ind v2) when v1 <> v2 -> false
	| (Ind v1, Ind v2) -> true
	in toBool (i1, i2)

(*
let assign pred var i = 
	match pred with
	| UP x -> if (x = var) then UP i else pred
	| BP (x,y) when x = y -> if (x = var) then BP (i, i) else pred
	| BP (x,y) when x = var -> BP (i, y)
	| BP (x,y) when y = var -> BP (x, i)
*)

let matrix ls = 
	let rec mtx ls1 ls2 = 
	match ls1 with 
	| [] -> []
	| [h] -> [(List.map (mkTuple h) ls2)]
	| h::tl -> (List.map (mkTuple h) ls2) :: (mtx tl ls2)
in mtx ls ls

let z_asg pr (u_ls : u list) = 
	let vars = gVL pr in
	let rec varInds ls1 ls2 =
	match ls1 with 
	| [] -> []
	| [h] -> [List.map ((|->) h) ls2]
	| h::tl -> (List.map ((|->) h) ls2) :: (varInds tl ls2)
in varInds vars u_ls

let z_asg1 pr i = 
	let vars = gVL pr in
	let rec tmpAsg ls f = 
	match ls with
	| [] -> []
	| [h] -> [ f h ]
	| h::tl -> ( f h ) :: (tmpAsg tl f) 
in tmpAsg vars (z i)

let z_asg2 pr i_ls = 
	let vars = gVL pr in
	let rec tmpAsg ls f = 
	match ls with
	| [] -> []
	| [h] -> [ f h ]
	| h::tl -> ( f h ) :: (tmpAsg tl f) 
	in let rec forEachInd ls = 
	match ls with
	| [] -> []
	| [h] -> [tmpAsg vars (z h)]
	| h::tl -> (tmpAsg vars (z h)) :: (forEachInd tl)
in forEachInd i_ls

let rec cut ls1 ls2 = 
	let rec subcut l1 l2 = 
			match l1 with
			| [] -> [l2]
			| [h] -> [List.hd l2] :: [(List.tl l2)]
			| h::tl -> ( [(List.hd l2)] ) :: (subcut tl (List.tl l2))  
		in let rec joinSub ls = match ls with
		| [] -> []
		| [h] -> [h]
		| f::s::[] -> [f;s]
		| f::s::tl -> joinSub  ((f @ s)::tl)
		in joinSub (subcut ls1 ls2)


let ( |-> ) (var : v) (ind : u)  = (var , ind)
let z (var : v) (ind : u)  = (var , ind)

let z_asg pr (i_ls : u list)  = 
	try
		let vars = gVL pr in
		match vars with
		| [] -> []
		| [h] -> [h |-> (List.hd i_ls)]
		| _ -> List.map2 (|->) vars i_ls
	with
		Invalid_argument _ -> []

let rec asg_u (u_ls : u list) f = 
	match u_ls with
	| [] -> []
	| [h] -> [f [h]]
	| h::tl -> (f [h]) :: (asg_u tl f)

let rec asg_b (u_ls : u list) f = 
	let pairs = matrix_ u_ls in
	match pairs with
	| [] -> []
	| [h] -> List.map f h

let singleAsg (p:pr) (k_ind: u list) = 
	match p with
	| Up (name,x) -> Ap (name, k_ind )
	| Bp (name,x,y) -> Ap (name, k_ind ) 


(* let interpret (act : action) (env : z) = 
	match act with
	| Set_null_l f -> f (gP env) (gEnv env)
	| Set_true_l f -> f (gP env) (gEnv env)


let rec initStruct (z_ls : z list list) (act : action) = 
	match z_ls with
	| [] -> []
	| [h] -> [ (List.combine h (List.map (interpret act) h) ) ]
	| h::tl -> (List.combine h (List.map (interpret act) h) ) :: (initStruct tl act)

let updateWith (a : action) (pr : p) (zs : (z * int) list list) =
	let rec tmpUpdateWith (act : action) (pred : p) (pairs : (z * int) list list) = 
		match pairs with
		| [] -> []
		| [h] -> if pred <=> (p_of h) 
		then [ (List.combine (decouple h) (List.map (interpret act) (decouple h)))] 
		else [h]
		| h::tl -> if pred <=> (p_of h) 
		then (List.combine (decouple h) (List.map (interpret act) (decouple h))) :: (tmpUpdateWith act pred tl)  
		else h :: (tmpUpdateWith act pred tl)
	in tmpUpdateWith a pr zs  *)


let set_null_lhs = 
	let f_action (p:p) = 0 in 
	Set_null_l f_action 



let decouple (i_tuples : (z * int) list) = fst (List.split i_tuples)
let p_of (tls : (z * int) list ) = gP (fst (List.hd tls))
let env_of (tls : z * int ) = gEnv (fst tls)

let mkStruct (universe : ind list) (act : action) =
	let assignments = ( actPred act ) |-> universe in
	let evaluations = (List.map (actFunc act) (assignments)) in
	let interpret (p:p) (ls : ind list) = 
in S ( universe , interpret )
