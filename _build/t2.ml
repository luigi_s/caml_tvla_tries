

(*type universe = Ind of string
type term = V of string | C of universe | F of term list
type predicate = P of term list
type connective = 
	  And of string
	| Or of string
	| Imp of string
	| Iff of string
	| Not of string
	| Eq of char
	| Bel of string
	| Foreach of string
	| Exists of string
	| Sucht of char

type formula = 
	  Atm of predicate
	| Bin of formula * connective * formula
	| Unr of connective * formula
*)

type ('a)formula = False
                 | True
                 | Atom 	of 'a
                 | Not 		of ('a)formula
                 | And 		of ('a)formula * ('a)formula
                 | Or 		of ('a)formula * ('a)formula
                 | Imp 		of ('a)formula * ('a)formula
                 | Iff 		of ('a)formula * ('a)formula
                 | Forall 	of string * ('a)formula
                 | Exists 	of string * ('a)formula;;

type token = 
		| TRUE
		| FALSE
		| VAR 	of string 
		| CONST of string
		| FUNC 	of string 
		| PRED 	of string
		| L_BRACK
		| R_BRACK
		| FORALL
		| EXISTS
		| AND
		| OR
		| IMP
		| IFF
		| NOT
		| COMMA
		| COLON
		| DOT
		| EOF








